package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.tools.ChainException;
import java.nio.file.Files;

/**
 * Represents put (buy or sell) order. It is a subclass of OrderCall.
 * @see OrderCall
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallPut extends OrderCall {

    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Order side: buy or sell
     */
    private OrderSide side;
    /**
     * Client putting order
     */
    private String client;
    /**
     * Product (asset) for requested operation.
     */
    private String product;
    /**
     * Order details: quantity and price.
     */
    private OrderDetail details;
    /**
     * Reference to modification order used in case when order is a result of modification action.
     */
    private OrderCallModify sourceRequest;
    //</editor-fold>

    public OrderCallPut() {
        this.details = new OrderDetail();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * When deserialization takes place all the indirect properties must be calculated.
     */
    @Override
    protected void initialize() {
        this.details.resetCurrentAmount();
    }

    /**
     * Execute processing of order in specific asset ladder.
     * @param ladder
     * @throws Exception 
     */
    @Override
    public void Process(AssetLadder ladder) throws Exception {
        getContext().setLadder(ladder);
        ladder.putOrder(this);
    }

    /**
     * Returns asset name.
     * @return  asset name.
     */
    @Override
    public String getAssetName() {
        return this.product;
    }

    /**
     * Lower current quantity by specific amount as a result of transaction.
     * @param amount
     * @return
     * @throws IllegalArgumentException
     * @throws ChainException 
     */
    public OrderCallPut consumeAmount(int amount) throws IllegalArgumentException, ChainException {
        OrderCallPut next = this;
        this.details.consumeAmount(amount);
        AssetOffer currentOffer = this.getContext().getOffer();
        if (null != currentOffer) {
            currentOffer = getContext().getOffer().consumeAmount(amount);
        }
        if (0 == this.details.getCurrentAmount()) {
            setStatus(OrderCallStatus.EXECUTED);
            next = (OrderCallPut) this.nextItem;
            excludeFromChain();
        }
        // if current offer it ran out of orders try to take first of the next offer
        if (null == next && null != currentOffer) {
            next = currentOffer.getOrderCallChainHead();
        }
        return next;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters/getters for class fields">
    /**
     * Getter for order side.
     * @return the side
     */
    public OrderSide getSide() {
        return side;
    }

    /**
     * Setter for order side.
     * @param side the side to set
     */
    public void setSide(OrderSide side) {
        this.side = side;
    }

    /**
     * Getter for client name.
     * @return the client
     */
    public String getClient() {
        return client;
    }

    /**
     * Setter for client name.
     * @param client the client to set
     */
    public void setClient(String client) {
        this.client = client;
    }

    /**
     * Getter for product name.
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * Setter for product name.
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * Getter for order details.
     * @return the details
     */
    public OrderDetail getDetails() {
        return details;
    }

    /**
     * Setter for order details.
     * @param details the details to set
     */
    public void setDetails(OrderDetail details) {
        this.details = details;
    }

    /**
     * Getter for source (modification order).
     * @return the sourceRequest
     */
    public OrderCallModify getSourceRequest() {
        return sourceRequest;
    }

    /**
     * Setter for source (modification order).
     * @param sourceRequest the sourceRequest to set
     */
    public void setSourceRequest(OrderCallModify sourceRequest) {
        this.sourceRequest = sourceRequest;
    }

    /**
     * Getter for offer which owns specific order.
     * @return 
     */
    public AssetOffer getOffer() {
        return getContext().getOffer();
    }
    // </editor-fold>

}
