package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.tools.ChainItem;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class AssetLadderTest {

    static Logger logger = LoggerFactory.getLogger(AssetOfferTest.class);

    public AssetLadderTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setChainHead method, of class AssetLadder.
     */
    @Test
    public void testSetChainHeadC001() throws Exception {
        logger.info("setChainHead C001: set null head");
        AssetLadder asset = new AssetLadder("ABC");
        try {
            asset.setChainHead(null);
            fail("Failed. You cannot set Chain Head as null.");
        } catch (Exception ex) {

        }
    }

    /**
     * Test of setChainTail method, of class AssetLadder.
     */
    @Test
    public void testSetChainTailC001() throws Exception {
        logger.info("setChainTail C001: set null tail");
        AssetLadder asset = new AssetLadder("ABC");
        try {
            asset.setChainTail(null);
            fail("Failed. You cannot set Chain Tail as null.");
        } catch (Exception ex) {

        }
    }
    
    /**
     * Test of getAssetName method, of class AssetLadder.
     */
    @Test
    public void testGetAssetName() {
        logger.info("getAssetName");
        String assetName = "ABC209";
        AssetLadder asset = new AssetLadder(assetName);
        assertEquals(assetName, asset.getAssetName());
    }

}
