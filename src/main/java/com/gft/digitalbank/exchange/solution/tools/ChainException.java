package com.gft.digitalbank.exchange.solution.tools;

/**
 *
 * @author Marcin Buciora (c) 2016
 */
public class ChainException extends Exception {
    protected ChainItem chainItem;
    protected ChainOwner chainOwner;
    public ChainException(String message, ChainItem item, ChainOwner owner) {
        super(message);
        this.chainItem = item;
        this.chainOwner = owner;
    }

    /**
     * @return the chainItem
     */
    public ChainItem getChainItem() {
        return chainItem;
    }

    /**
     * @return the chainOwner
     */
    public ChainOwner getChainOwner() {
        return chainOwner;
    }
}
