package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.solution.engine.OrderCall;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Interface for objects responsible for keeping assets data. They need to provide a reference
 * for order queue.
 * @see StockExchange
 * @author Marcin Buciora (c) 2016
 */
public interface AssetContainer {
    /**
     * Returns a reference to order queue.
     * @param assetName name of the asset
     * @return reference to order queue
     */
    public ConcurrentLinkedQueue<OrderCall> getAssetQueue(String assetName);
    public int getClock();
    public void moveClock();
}
