package com.gft.digitalbank.exchange.solution.tools;

/**
 * An abstraction that allows chain items [{@link ChainItem}] to keep chain
 * owner updated and consistent. Chain owner keeps at least a reference of chain
 * head (it can keep a chain tail reference too). Chain modifications are
 * executed by its items, so there is a interface back-link to chain owner which
 * allows to keep references stored in chain owner up to date. Each class where
 * you want to use chain item solution has to implement this interface.
 *
 * @author Marcin Buciora (c) 2016
 */
public interface ChainOwner {

    /**
     * Update chain head with reference pointed by parameter. Chain owner must
     * update its data.
     *
     * @param newHead reference to item that is new chain head
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void setChainHead(ChainItem newHead) throws ChainException;

    /**
     * Update chain tail with reference pointed by parameter. Chain owner must
     * update its data.
     *
     * @param newTail reference to item that is new chain tail
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void setChainTail(ChainItem newTail) throws ChainException;

    /**
     * When chain collapses during removing its last element
     * [{@link ChainItem#excludeFromChain()}], chain owner is notified with this
     * method about the case. The chain owner can identify which chain collapses
     * by received reference to head item. Chain owner must clear its references
     * to head and tail of the chain.
     *
     * @param chainId
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     * @see ChainItem
     * @see ChainItem#excludeFromChain()
     */
    public void clearChain(int chainId) throws ChainException;

}
