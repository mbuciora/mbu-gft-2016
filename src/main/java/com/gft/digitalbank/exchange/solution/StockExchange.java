package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.Exchange;
import com.gft.digitalbank.exchange.listener.ProcessingListener;
import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.SolutionResult;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.engine.AssetLadder;
import com.gft.digitalbank.exchange.solution.engine.OrderCall;
import com.gft.digitalbank.exchange.solution.engine.OrderCallShutdown;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * StockExchange represents a heart of solution. It Manage all the objects,
 * communicates with message engine, keeps all stock data and provide
 * transaction results. StockExchange creates and keeps message processors
 * [{@link BrokerCallProcessor}] and asset cards [{@link AssetCard}]. When it
 * get notification that last processor has end its work, StockExchange get all
 * the results and forward them as well as watch threads to close and resources
 * to close.
 *
 * @author Marcin Buciora (c) 2016
 */
public class StockExchange implements Exchange, CallProcessedEventListener, AssetContainer {

    //<editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * External listener waiting for solution to complete processing.
     */
    private ProcessingListener listener;
    /**
     * Collection of jms destination names.
     */
    private List<String> destinationNames;
    /**
     * Collection of all processors built during solution work.
     */
    private final List<BrokerCallProcessor> processors;
    /**
     * Context universal key to separate many StockExchange instances in
     * multi-thread environment
     */
    private final UUID applicationContext;

    /**
     * Reference to jms connection
     */
    private Connection connection;
    /**
     * Collection of asset ladders. Mapped by asset name.
     */
    private final Map<String, AssetCard> assetCards;
    /**
     * Reference to executor which is responsible for all callable objects. In
     * this case they are AssetProcessWrapper objects.
     */
    private final ExecutorService assetExecutor;
   /**
     * Simple solution for time coordination in case when orders are received
     * not along with their timestamp.
     */
    private int clock;
    /**
     * Global logger.
     */
    static Logger logger = LoggerFactory.getLogger(StockExchange.class);
    private Boolean isRunning;
    //</editor-fold>

    public StockExchange() {
        // context is important to keep order if many StockExchange instances is used inside multithread platform...
        this.applicationContext = UUID.randomUUID();
        this.destinationNames = new ArrayList<>();
        this.processors = new ArrayList<>();
        this.assetCards = new HashMap<>();
        this.assetExecutor = Executors.newCachedThreadPool();
        this.isRunning = false;
    }

    //<editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Register the external listener
     *
     * @param processingListener
     */
    @Override
    public void register(ProcessingListener processingListener) {
        this.listener = processingListener;
    }

    /**
     * Allow to set destination names - each destination is a pipeline to a
     * broker office.
     *
     * @param list
     */
    @Override
    public void setDestinations(List<String> list) {
        this.destinationNames = list;
    }

    /**
     * Start stock engine work. Get jms work, setup processors.
     */
    @Override
    public void start() {
        logger.info("Stock engine start.");
        
        if ( 0 == this.destinationNames.size() || null == this.listener) return;
        try {
            if ( true == this.isRunning) throw new Exception("StockExchange already running.");
            this.isRunning = true;
            IoCContainer.get().registerInstance(this.applicationContext, AssetContainer.class, this);

            Context context = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");

            this.connection = connectionFactory.createConnection();
            this.connection.start();

            IoCContainer.get().registerInstance(this.applicationContext, Connection.class, this.connection);

            for (String dst : this.destinationNames) {
                BrokerCallProcessor processor = new BrokerCallProcessor(this.applicationContext, dst);
                processor.addListener(this);
                processors.add(processor);
            }
            
            // reset clock to start value
            this.clock = 1;
            for(BrokerCallProcessor p : processors){
                p.start();
            }
            
        } catch (Exception ex) {
            logger.error("Stock engine start error:", ex);
            try {
                if (this.connection != null) {
                    this.connection.stop();
                    this.connection.close();
                }
            } catch (JMSException jmse) {
                logger.error("Stock engine error. See details.", ex);
            }
            logger.error("Stock engine error. See details.", ex);
        }
    }

    /**
     * Deal with events from processors. When last processor notify its job end
     * then prepare all results and feed them forward. Shutdown all used
     * resources.
     *
     * @param event notification event from processor
     */
    @Override
    public synchronized void callProcessedEventReceived(CallProcessedEvent event) {
        try {
            processors.remove(event.getSource());

            if (processors.isEmpty()) {
                this.connection.stop();
                this.connection.close();

                // gather all results
                Set<Transaction> transactions = new HashSet<>();
                Set<OrderBook> orderBooks = new HashSet<>();

                OrderCallShutdown call = new OrderCallShutdown();
                for (String key : this.assetCards.keySet()) {
                    this.assetCards.get(key).getQueue().add(call);

                    //blocks until future data ready for receive...
                    Future<AssetLadder> future = this.assetCards.get(key).getFuture();
                    while (false == future.isDone()) {
                    }

                    AssetLadder asset = future.get();
                    transactions.addAll(asset.getTransactions());
                    OrderBook ob = asset.buildOrderBook();
                    if (null != ob) {
                        orderBooks.add(ob);
                    }
                }
                IoCContainer.get().clean(this.applicationContext);

                // build response
                SolutionResult sr = SolutionResult.builder()
                        .transactions(transactions)
                        .orderBooks(orderBooks)
                        .build();

                this.listener.processingDone(sr);

                this.assetExecutor.shutdown();
                while (true != this.assetExecutor.isShutdown()) {
                }

            }
        } catch (JMSException | InterruptedException | ExecutionException ex) {
            logger.error("Stock engine error. See details.", ex);
        } finally {

            logger.info("Stock engine stop.");
        }
    }

    /**
     * Return a reference to existing queue to asset wrapper or creates a new
     * wrapper and return its queue. It is used to feed incoming order calls. In
     * our case StockExchange object keeps all references to asset ladder
     * wrappers. It implements AssetContainer interface. Its reference is
     * accessible by IoC container.
     *
     * @param assetName
     * @return reference to queue
     */
    @Override
    public ConcurrentLinkedQueue<OrderCall> getAssetQueue(String assetName) {
        AssetCard card = assetCards.get(assetName);
        if (null == card) {
            synchronized (this.assetCards) {
                card = new AssetCard(assetName);
                this.assetCards.put(assetName, card);
                card.setFuture(
                        this.assetExecutor.submit(
                                new AssetProcessWrapper(
                                        card.getAssetName(),
                                        card.getQueue()
                                )
                        )
                );
            }
        }
        return card.getQueue();
    }
    
    @Override
    public synchronized int getClock(){
        return this.clock;
    }
    
    @Override
    public synchronized void moveClock(){
        this.clock++;
    }
    //</editor-fold>

    /**
     * Simple wrapper for keeping references of queue and future together with
     * asset name. Owner instead of keeping reference to callable keeps
     * reference to its queue and its future.
     *
     * @author Marcin Buciora (c) 2016
     */
    private class AssetCard {

        // <editor-fold defaultstate="collapsed" desc="Class fields">
        /**
         * Asset name.
         */
        private final String assetName;
        /**
         * Order queue used by processor to feed order call to asset ladder.
         */
        private final ConcurrentLinkedQueue<OrderCall> queue;
        /**
         * Reference to result that will be provided by callable after finishing
         * the job.
         */
        private Future<AssetLadder> future;
        //</editor-fold>

        public AssetCard(String assetName) {
            this.assetName = assetName;
            this.queue = new ConcurrentLinkedQueue<>();
        }

        // <editor-fold defaultstate="collapsed" desc="Getters/Setters">
        /**
         * Getter for asset name
         *
         * @return the assetName
         */
        public String getAssetName() {
            return assetName;
        }

        /**
         * Getter for queue reference.
         *
         * @return the queue
         */
        public ConcurrentLinkedQueue<OrderCall> getQueue() {
            return queue;
        }

        /**
         * Getter for future reference
         *
         * @return the future
         */
        public Future<AssetLadder> getFuture() {
            return future;
        }

        /**
         * Setter for future reference.
         *
         * @return the future
         */
        protected void setFuture(Future<AssetLadder> future) {
            if (null == this.future) {
                this.future = future;
            }
        }
        //</editor-fold>
    }
}
