
package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;

/**
 * Represents modification order. It is a subclass of OrderCall.
 * Keeps reference to order mean to be modified.
 * @see OrderCall
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallModify extends OrderCall {
    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Id of order mean to be modified.
     */
    private int modifiedOrderId;
    /**
     * Information about modification details.
     */
    private OrderDetail details;
    /**
     * Reference to order mean to be modified. Reference is populated when processor execute its part.
     */
    private OrderCallPut referenceOrder;
    //</editor-fold>
    
    public OrderCallModify() {
        this.details = new OrderDetail();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * When deserialization takes place all the indirect properties must be calculated.
     */
    @Override
    protected void initialize(){
        this.details.resetCurrentAmount();
    }

    /**
     * Execute processing for this order. Get offer conected to modified order and run modification action.
     * It is possible only on BOOKED order.
     * @param ladder
     * @throws Exception 
     */
    @Override
    public void Process(AssetLadder ladder) throws Exception {
         if (OrderCallStatus.BOOKED == referenceOrder.getStatus()) {
            AssetOffer offer = referenceOrder.getContext().getOffer();
            offer.modifyOrderCall(referenceOrder, this);
        }
    }
    
    /**
     * Get name of asset for cancel order. Take it from modified order reference.
     * @return 
     */
    @Override
    public String getAssetName(){
        return this.referenceOrder.getAssetName();
    }
    
    /**
     * Setup references in modification order. Do as base class want and get reference to order mean to be
     * modified.
     * @param processor 
     */
    @Override
    public void SetupReferences(BrokerCallProcessor processor) {
        getContext().setProcessor(processor);
        referenceOrder = (OrderCallPut)getContext().getProcessor().getOrderCall(this.modifiedOrderId);
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Setters/getters for class fields">
    /**
     * Getter for modified order id.
     * @return the modifiedOrderId
     */
    public int getModifiedOrderId() {
        return modifiedOrderId;
    }

    /**
     * Setter for modified order id.
     * @param modifiedOrderId the modifiedOrderId to set
     */
    public void setModifiedOrderId(int modifiedOrderId) {
        this.modifiedOrderId = modifiedOrderId;
    }

    /**
     * Getter for order details.
     * @return the details
     */
    public OrderDetail getDetails() {
        return details;
    }


    /**
     * Getter for reference order.
     * @return the referenceOrder
     */
    public OrderCallPut getReferenceOrder() {
        return referenceOrder;
    }
    //</editor-fold>
}
