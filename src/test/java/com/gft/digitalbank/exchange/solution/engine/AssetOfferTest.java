package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.StockExchange;
import com.gft.digitalbank.exchange.solution.tools.ChainException;
import com.gft.digitalbank.exchange.solution.tools.ChainItem;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class AssetOfferTest {
    static Logger logger = LoggerFactory.getLogger(AssetOfferTest.class);
    private AssetLadder assetLadder;
    private OrderCallProvider provider;

    public AssetOfferTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {

    }

    @Before
    public void setUp() {
        this.assetLadder = new AssetLadder("XYZ");
        provider = new OrderCallProvider();
    }

    @After
    public void tearDown() {

    }

    /**
     * Test of incorporateOrderCall method, of class AssetOffer.
     */
    @Test
    public void testIncorporateOrderCallC001() throws Exception {
        logger.info("incorporateOrderCall C001: four calls increasing value");
        OrderCallPut order1 = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "XYZ", "C1", "B1");
        OrderCallPut order2 = this.provider.GenerateOrderCallPut(110, 20, OrderSide.SELL, "XYZ", "C2", "B1");
        OrderCallPut order3 = this.provider.GenerateOrderCallPut(120, 30, OrderSide.SELL, "XYZ", "C3", "B1");
        OrderCallPut order4 = this.provider.GenerateOrderCallPut(130, 40, OrderSide.SELL, "XYZ", "C4", "B1");

        AssetOffer offer = new AssetOffer(this.assetLadder, order1);
        offer.incorporateOrderCall(order2);
        offer.incorporateOrderCall(order3);
        offer.incorporateOrderCall(order4);

        assertEquals(OrderSide.SELL, offer.getSide());
        assertEquals(10, offer.getPrice());
        assertEquals(100, offer.getAmount());
        assertEquals(null, offer.getPrevItem());
        assertEquals(true, offer.isChainHead());
        assertEquals(false, offer.isChainTail());

        AssetOffer suboffer1 = (AssetOffer) offer.getNextItem();
        assertEquals(OrderSide.SELL, suboffer1.getSide());
        assertEquals(20, suboffer1.getPrice());
        assertEquals(110, suboffer1.getAmount());
        assertEquals(offer, suboffer1.getPrevItem());

        AssetOffer suboffer2 = (AssetOffer) suboffer1.getNextItem();
        assertEquals(OrderSide.SELL, suboffer2.getSide());
        assertEquals(30, suboffer2.getPrice());
        assertEquals(120, suboffer2.getAmount());
        assertEquals(suboffer1, suboffer2.getPrevItem());

        AssetOffer suboffer3 = (AssetOffer) suboffer2.getNextItem();
        assertEquals(OrderSide.SELL, suboffer3.getSide());
        assertEquals(40, suboffer3.getPrice());
        assertEquals(130, suboffer3.getAmount());
        assertEquals(suboffer2, suboffer3.getPrevItem());
        assertEquals(true, suboffer3.isChainTail());
        assertEquals(null, suboffer3.getNextItem());
    }

    /**
     * Test of incorporateOrderCall method, of class AssetOffer.
     */
    @Test
    public void testIncorporateOrderCallC002() throws Exception {
        logger.info("incorporateOrderCall C002: four calls decreasing value");
        OrderCallPut order1 = this.provider.GenerateOrderCallPut(100, 500, OrderSide.SELL, "XYZ", "C1", "B1");
        OrderCallPut order2 = this.provider.GenerateOrderCallPut(110, 400, OrderSide.SELL, "XYZ", "C2", "B1");
        OrderCallPut order3 = this.provider.GenerateOrderCallPut(120, 300, OrderSide.SELL, "XYZ", "C3", "B1");
        OrderCallPut order4 = this.provider.GenerateOrderCallPut(130, 200, OrderSide.SELL, "XYZ", "C4", "B1");

        AssetOffer offer = new AssetOffer(this.assetLadder, order1);
        offer.incorporateOrderCall(order2);
        offer.incorporateOrderCall(order3);
        offer.incorporateOrderCall(order4);

        assertEquals(OrderSide.SELL, offer.getSide());
        assertEquals(500, offer.getPrice());
        assertEquals(100, offer.getAmount());
        assertEquals(null, offer.getNextItem());
        assertEquals(false, offer.isChainHead());
        assertEquals(true, offer.isChainTail());

        AssetOffer suboffer1 = (AssetOffer) offer.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer1.getSide());
        assertEquals(400, suboffer1.getPrice());
        assertEquals(110, suboffer1.getAmount());
        assertEquals(offer, suboffer1.getNextItem());

        AssetOffer suboffer2 = (AssetOffer) suboffer1.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer2.getSide());
        assertEquals(300, suboffer2.getPrice());
        assertEquals(120, suboffer2.getAmount());
        assertEquals(suboffer1, suboffer2.getNextItem());

        AssetOffer suboffer3 = (AssetOffer) suboffer2.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer3.getSide());
        assertEquals(200, suboffer3.getPrice());
        assertEquals(130, suboffer3.getAmount());
        assertEquals(suboffer2, suboffer3.getNextItem());
        assertEquals(true, suboffer3.isChainHead());
        assertEquals(null, suboffer3.getPrevItem());
    }

    /**
     * Test of incorporateOrderCall method, of class AssetOffer.
     */
    @Test
    public void testIncorporateOrderCallC003() throws Exception {
        logger.info("incorporateOrderCall C003: four calls decreasing miexd value");
        OrderCallPut order1 = this.provider.GenerateOrderCallPut(100, 500, OrderSide.SELL, "XYZ", "C1", "B1");
        OrderCallPut order2 = this.provider.GenerateOrderCallPut(110, 300, OrderSide.SELL, "XYZ", "C2", "B1");
        OrderCallPut order3 = this.provider.GenerateOrderCallPut(120, 400, OrderSide.SELL, "XYZ", "C3", "B1");
        OrderCallPut order4 = this.provider.GenerateOrderCallPut(130, 450, OrderSide.SELL, "XYZ", "C4", "B1");

        AssetOffer offer = new AssetOffer(this.assetLadder, order1);
        offer.incorporateOrderCall(order2);
        offer.incorporateOrderCall(order3);
        offer.incorporateOrderCall(order4);

        assertEquals(OrderSide.SELL, offer.getSide());
        assertEquals(500, offer.getPrice());
        assertEquals(100, offer.getAmount());
        assertEquals(null, offer.getNextItem());
        assertEquals(false, offer.isChainHead());
        assertEquals(true, offer.isChainTail());

        AssetOffer suboffer1 = (AssetOffer) offer.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer1.getSide());
        assertEquals(450, suboffer1.getPrice());
        assertEquals(130, suboffer1.getAmount());
        assertEquals(offer, suboffer1.getNextItem());

        AssetOffer suboffer2 = (AssetOffer) suboffer1.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer2.getSide());
        assertEquals(400, suboffer2.getPrice());
        assertEquals(120, suboffer2.getAmount());
        assertEquals(suboffer1, suboffer2.getNextItem());

        AssetOffer suboffer3 = (AssetOffer) suboffer2.getPrevItem();
        assertEquals(OrderSide.SELL, suboffer3.getSide());
        assertEquals(300, suboffer3.getPrice());
        assertEquals(110, suboffer3.getAmount());
        assertEquals(suboffer2, suboffer3.getNextItem());
        assertEquals(true, suboffer3.isChainHead());
        assertEquals(null, suboffer3.getPrevItem());
    }

    /**
     * Test of incorporateOrderCall method, of class AssetOffer.
     */
    @Test
    public void testIncorporateOrderCallC004() throws Exception {
        logger.info("incorporateOrderCall C004: four calls increasing mixed value");
        OrderCallPut order1 = this.provider.GenerateOrderCallPut(100, 10, OrderSide.BUY, "XYZ", "C1", "B1");
        OrderCallPut order2 = this.provider.GenerateOrderCallPut(110, 40, OrderSide.BUY, "XYZ", "C2", "B1");
        OrderCallPut order3 = this.provider.GenerateOrderCallPut(120, 30, OrderSide.BUY, "XYZ", "C3", "B1");
        OrderCallPut order4 = this.provider.GenerateOrderCallPut(130, 35, OrderSide.BUY, "XYZ", "C4", "B1");

        AssetOffer offer = new AssetOffer(this.assetLadder, order1);
        offer.incorporateOrderCall(order2);
        offer.incorporateOrderCall(order3);
        offer.incorporateOrderCall(order4);
        while (false == offer.isChainHead()) {
            offer = (AssetOffer) offer.getPrevItem();
        }

        assertEquals(OrderSide.BUY, offer.getSide());
        assertEquals(40, offer.getPrice());
        assertEquals(110, offer.getAmount());
        assertEquals(null, offer.getPrevItem());
        assertEquals(true, offer.isChainHead());
        assertEquals(false, offer.isChainTail());

        AssetOffer suboffer1 = (AssetOffer) offer.getNextItem();
        assertEquals(OrderSide.BUY, suboffer1.getSide());
        assertEquals(35, suboffer1.getPrice());
        assertEquals(130, suboffer1.getAmount());
        assertEquals(offer, suboffer1.getPrevItem());

        AssetOffer suboffer2 = (AssetOffer) suboffer1.getNextItem();
        assertEquals(OrderSide.BUY, suboffer2.getSide());
        assertEquals(30, suboffer2.getPrice());
        assertEquals(120, suboffer2.getAmount());
        assertEquals(suboffer1, suboffer2.getPrevItem());

        AssetOffer suboffer3 = (AssetOffer) suboffer2.getNextItem();
        assertEquals(OrderSide.BUY, suboffer3.getSide());
        assertEquals(10, suboffer3.getPrice());
        assertEquals(100, suboffer3.getAmount());
        assertEquals(suboffer2, suboffer3.getPrevItem());
        assertEquals(true, suboffer3.isChainTail());
        assertEquals(null, suboffer3.getNextItem());
    }

    /**
     * Test of setChainHead method, of class AssetOffer.
     */
    @Test(expected = ChainException.class)
    public void testSetChainHead() throws Exception {
        logger.info("setChainHead");

        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.BUY, "XYZ", "C1", "B1");
        AssetOffer offer = new AssetOffer(this.assetLadder, order);

        offer.setChainHead(null);
        fail("Incorrect, chain head cannot accept null.");
    }

    /**
     * Test of setChainTail method, of class AssetOffer.
     */
    @Test(expected = ChainException.class)
    public void testSetChainTail() throws Exception {
        logger.info("setChainTail");

        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.BUY, "XYZ", "C1", "B1");
        AssetOffer offer = new AssetOffer(this.assetLadder, order);

        offer.setChainTail(null);
        fail("Incorrect, chain tail cannot accept null.");
    }

    /**
     * Test of clearChain method, of class AssetOffer.
     */
    @Test
    public void testClearChain() throws Exception {
        logger.info("clearChain");

    }

    /**
     * Test of getSide method, of class AssetOffer.
     */
    @Test
    public void testGetSide() {
        logger.info("getSide");

        try {
            OrderCallPut order = this.provider.GenerateOrderCallPut(130, 35, OrderSide.BUY, "XYZ", "C4", "B1");
            AssetOffer offer = new AssetOffer(this.assetLadder, order);
            assertEquals(OrderSide.BUY, offer.getSide());

            order = this.provider.GenerateOrderCallPut(13, 5, OrderSide.SELL, "XYZ", "C4", "B1");
            offer = new AssetOffer(this.assetLadder, order);
            assertEquals(OrderSide.SELL, offer.getSide());
            
        } catch (Exception ex) {
            fail("Unexpected exception received. " + ex.getMessage());
        }
    }

    /**
     * Test of getOrderCallChainTail method, of class AssetOffer.
     */
    @Test
    public void testGetOrderCallChainTail() {
        logger.info("getOrderCallChainTail");

        try {
        OrderCallPut order1 = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "XYZ", "C1", "B1");
        OrderCallPut order2 = this.provider.GenerateOrderCallPut(110, 20, OrderSide.SELL, "XYZ", "C2", "B1");

        AssetOffer offer = new AssetOffer(this.assetLadder, order1);
        offer.incorporateOrderCall(order2);
        assertNotNull(null, offer.getOrderCallChainTail());
        
        } catch (Exception ex){
            fail("Unexpected exception received. "+ex.getMessage());
        }
    }

}
