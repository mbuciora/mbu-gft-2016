package com.gft.digitalbank.exchange.solution;

import static com.gft.digitalbank.exchange.solution.StockExchangeTest.logger;
import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

/**
 * @author Marcin Buciora (c) 2016
 */
public class IoCContainerTest {

    public IoCContainerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
        IoCContainer ioc = IoCContainer.get();
        ioc.clean();
    }

    /**
     * Test of get method, of class IoCContainer.
     */
    @Test
    public void testGetC001() {
        IoCContainer ioc1 = IoCContainer.get();
        IoCContainer ioc2 = IoCContainer.get();
        IoCContainer ioc3 = IoCContainer.get();
        assertNotEquals(null, ioc1);
        assertNotEquals(null, ioc2);
        assertNotEquals(null, ioc3);
        assertEquals(ioc1, ioc2);
        assertEquals(ioc1, ioc3);
    }

    /**
     *
     */
    @Test
    public void testRegisterInstanceC001() {
        logger.info("registerInstance C001: one type and instance");
        UUID applicationContext = UUID.randomUUID();
        AssetContainer assets = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        ioc.registerInstance(applicationContext, AssetContainer.class, assets);

        AssetContainer found = ioc.getInstance(applicationContext, AssetContainer.class);
        assertEquals(assets, found);

    }

    /**
     * Test of registerInstance method, of class IoCContainer.
     */
    @Test
    public void testRegisterInstanceC002() {
        logger.info("registerInstance C002: nulls");
        UUID applicationContext = UUID.randomUUID();

        AssetContainer assets = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        try {
            ioc.registerInstance(applicationContext, AssetContainer.class, null);
            fail("Null instance has been accepted.");
        } catch (IllegalArgumentException ex) {
        }

        try {
            ioc.registerInstance(applicationContext, null, assets);
            fail("Null class has been accepted.");
        } catch (IllegalArgumentException ex) {
        }

        try {
            ioc.registerInstance(applicationContext, null, null);
            fail("Null instance and class have been accepted.");
        } catch (IllegalArgumentException ex) {
        }

    }

    /**
     * Test of registerInstance method, of class IoCContainer.
     */
    @Test
    public void testRegisterInstanceC003() {
        logger.info("registerInstance C003: doubled instance for the class");
        UUID applicationContext = UUID.randomUUID();
        AssetContainer assets1 = Mockito.mock(AssetContainer.class);
        AssetContainer assets2 = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        try {
            ioc.registerInstance(applicationContext, AssetContainer.class, assets1);
            ioc.registerInstance(applicationContext, AssetContainer.class, assets2);
            fail("Doubled instance has been accepted.");
        } catch (IllegalArgumentException ex) {
        }

        AssetContainer found = ioc.getInstance(applicationContext, AssetContainer.class);
        assertEquals(found, assets1);

    }

    /**
     * Test of registerInstance method, of class IoCContainer.
     */
    @Test
    public void testRegisterInstanceC004() {
        logger.info("registerInstance C004: different application contexts");
        UUID applicationContext1 = UUID.randomUUID();
        UUID applicationContext2 = UUID.randomUUID();
        AssetContainer assets1 = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        try {
            ioc.registerInstance(applicationContext1, AssetContainer.class, assets1);
            ioc.registerInstance(applicationContext2, AssetContainer.class, assets1);
        } catch (IllegalArgumentException ex) {
            fail("Application context not separated.");
        }

        AssetContainer found = ioc.getInstance(applicationContext1, AssetContainer.class);
        assertEquals(found, assets1);
        found = ioc.getInstance(applicationContext2, AssetContainer.class);
        assertEquals(found, assets1);
    }

    /**
     * Test of getInstance method, of class IoCContainer.
     */
    @Test
    public void testGetInstanceC001() {
        logger.info("getInstance C001: One class");
        UUID applicationContext = UUID.randomUUID();
        AssetContainer assets = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        ioc.registerInstance(applicationContext, AssetContainer.class, assets);

        AssetContainer found = ioc.getInstance(applicationContext, AssetContainer.class);
        assertEquals(assets, found);
    }

    /**
     * Test of clean method, of class IoCContainer.
     */
    @Test
    public void testCleanC001() {
        logger.info("clean C001: cleaning");
        UUID applicationContext = UUID.randomUUID();
        AssetContainer assets = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        ioc.registerInstance(applicationContext, AssetContainer.class, assets);

        AssetContainer found = ioc.getInstance(applicationContext, AssetContainer.class);
        assertEquals(assets, found);

        ioc.clean(applicationContext);

        found = ioc.getInstance(applicationContext, AssetContainer.class);
        assertEquals(null, found);
    }

    /**
     * Test of clean method, of class IoCContainer.
     */
    @Test
    public void testCleanC002() {
        logger.info("clean C002: cleaning specific application context");
        UUID applicationContext1 = UUID.randomUUID();
        UUID applicationContext2 = UUID.randomUUID();
        AssetContainer assets = Mockito.mock(AssetContainer.class);
        IoCContainer ioc = IoCContainer.get();

        assertNotEquals(null, ioc);

        ioc.registerInstance(applicationContext1, AssetContainer.class, assets);
        ioc.registerInstance(applicationContext2, AssetContainer.class, assets);

        AssetContainer found = ioc.getInstance(applicationContext1, AssetContainer.class);
        assertEquals(assets, found);

        ioc.clean(applicationContext1);

        found = ioc.getInstance(applicationContext1, AssetContainer.class);
        assertEquals(null, found);

        found = ioc.getInstance(applicationContext2, AssetContainer.class);
        assertEquals(assets, found);

    }
}
