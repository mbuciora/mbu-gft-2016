package com.gft.digitalbank.exchange.solution.engine;

/**
 * Enumeration that represents side of asset operation. Order can be for buy or
 * sell security.
 * @author Marcin Buciora (c) 2016
 * @version 1.0.0
 */
public enum OrderSide {
    /**
     * Indeterminate side.
     */
    UNKNOWN,
    /**
     * Buy side.
    */
    BUY,
    /**
     * Sell side.
     */
    SELL;

    /**
     * Converter that translate string value to enumeration value.
     * In case of undefined result it returns UNKNOWN.
     * @param name order side expressed as string
     * @return order side converted to enumeration value
     */
    public static OrderSide fromString(String name) {
        switch (name) {
            case "BUY":
                return BUY;
            case "SELL":
                return SELL;
            default:
                return UNKNOWN;
        }
    }

    /**
     * Converter that translate integer value to enumeration value.
     * In case of undefined result it returns UNKNOWN.
     * @param value order side expressed as integer value
     * @return order side converted to enumeration value
     */
    public static OrderSide fromInt(int value) {
        switch (value) {
            case 1:
                return BUY;
            case 2:
                return SELL;
            default:
                return UNKNOWN;
        }
    }

    /**
     * Converter that translate enumeration value to integer value.
     * In case of undefined result it returns 0.
     * @param value order side expressed as enumeration value
     * @return order side converted to integer value
     */
    public static int toInt(OrderSide value) {
        switch (value) {
            case BUY:
                return 1;
            case SELL:
                return 2;
            default:
                return 0;
        }
    }
}
