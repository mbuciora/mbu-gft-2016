
package com.gft.digitalbank.exchange.solution;

/**
 * Interface for processor listeners. All are notified when processor finish its order processing job.
 * @author  Marcin Buciora (c) 2016
 */
public interface CallProcessedEventListener {
    public void callProcessedEventReceived(CallProcessedEvent event);
}
