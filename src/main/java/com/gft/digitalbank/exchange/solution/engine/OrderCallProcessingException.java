package com.gft.digitalbank.exchange.solution.engine;

/**
 * Exception for specific errors that can occure during order call processing.
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallProcessingException extends Exception {
    /**
     * Oryginal exception.
     */
    protected Exception sourceException;
    /**
     * Processing context details.
     */
    protected ProcessingContext context;
    /**
     * Order whose processing throws exception.
     */
    protected OrderCall order;
    /**
     * Custom message for exception situation.
     */
    protected String message;
    
    public OrderCallProcessingException(String message, Exception exception, ProcessingContext context, OrderCall order){
        super(message);
        this.message = message;
        this.sourceException = exception;
        this.context = context;
        this.order = order;
    }

    /**
     * Getter for source exception.
     * @return the sourceException
     */
    public Exception getSourceException() {
        return sourceException;
    }

    /**
     * Getter for processing context.
     * @return the context
     */
    public ProcessingContext getContext() {
        return context;
    }

    /**
     * Getter for order reference.
     * @return the order
     */
    public OrderCall getOrder() {
        return order;
    }

    /**
     * Getter for custom message.
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }
    
}
