package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.solution.engine.AssetLadder;
import com.gft.digitalbank.exchange.solution.engine.OrderCall;
import com.gft.digitalbank.exchange.solution.engine.OrderCallShutdown;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class is used to operate on asset ladder object and deal with multi-threading.
 * It is a wrapper around ladder object. Wrapper is used to work on concurrent queue and feed ladder
 * with received orders. Wrapper implements Callable interface.
 * @author Marcin Buciora (c) 2016
 * @see AssetLadder
 * @see Callable
 */
public class AssetProcessWrapper implements Callable<AssetLadder> {
    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Reference to asset ladder [{link AssetLadder}].
     */
    private final AssetLadder ladder;
    /**
     * Reference to order queue waiting for processing in the ladder [{link OrderCall}].
     */
    private final ConcurrentLinkedQueue<OrderCall> callQueue;
    //</editor-fold>

    public AssetProcessWrapper(String assetName, ConcurrentLinkedQueue<OrderCall> callQueue) {
        this.ladder = new AssetLadder(assetName);
        this.callQueue = callQueue;
    }

    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Main flow where orders are taken from queue and feed to asset ladder object.
     * It manage process state by watching for shutdown order call. If such received then thread
     * is closing and return asset ladder object as a callable result.
     * @return processed asset ladder object.
     * @throws Exception 
     */
    @Override
    public AssetLadder call() throws Exception {
        Boolean process = true;
        while (true == process) {
            OrderCall call = this.callQueue.poll();
            if (null != call) {
                try {
                    if (false == call.getClass().isAssignableFrom(OrderCallShutdown.class)) {
                        this.ladder.processCall(call);
                    } else {
                        process = false;
                    }
                } catch (Exception ex) {
                    throw ex;
                }
            }
        }
        return this.ladder;
    }
    //</editor-fold>
}
