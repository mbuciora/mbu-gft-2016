package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallCancelTest {
    
    public OrderCallCancelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setCancelledOrderId method, of class OrderCallCancel.
     */
    @Test
    public void testSetCancelledOrderId() {
        OrderCallCancel order = new OrderCallCancel();
        int cancelledOrderId = 90;
        
        order.setCancelledOrderId(cancelledOrderId);
        assertEquals(cancelledOrderId, order.getCancelledOrderId());
    }
    
}
