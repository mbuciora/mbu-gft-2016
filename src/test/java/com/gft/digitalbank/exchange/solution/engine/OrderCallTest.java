package com.gft.digitalbank.exchange.solution.engine;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallTest {

    static Logger logger = LoggerFactory.getLogger(OrderCallTest.class);

    public OrderCallTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of CallBuilder method, of class OrderCall.
     */
    @Test
    public void testCallBuilderC001() {
        logger.info("CallBuilder C001: valid buy test message");
        try {
            // <editor-fold defaultstate="collapsed" desc="Order-BUY">
            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "ORDER");
            msg.setText("{\"client\":\"100\",\"product\":\"A\",\"side\":\"BUY\",\"details\":{\"amount\":100,\"price\":100},\"messageType\":\"ORDER\",\"timestamp\":1,\"id\":1,\"broker\":\"1\"}");
            OrderCall order = OrderCall.CallBuilder(msg);

            assertTrue(order.getClass().isAssignableFrom(OrderCallPut.class));
            OrderCallPut putOrder = (OrderCallPut) order;
            assertEquals("100", putOrder.getClient());
            assertEquals("A", putOrder.getProduct());
            assertEquals(OrderSide.BUY, putOrder.getSide());
            assertEquals(100, putOrder.getDetails().getAmount());
            assertEquals(100, putOrder.getDetails().getPrice());
            assertEquals("ORDER", putOrder.getMessageType());
            assertEquals(1, putOrder.getTimestamp());
            assertEquals(1, putOrder.getId());
            assertEquals("1", putOrder.getBroker());
            //</editor-fold>
        } catch (JMSException e) {
            fail("Test case error - fail.");
        } catch (OrderCallProcessingException e) {
            fail("Test case error - fail.");
        }
    }

    /**
     * Test of CallBuilder method, of class OrderCall.
     */
    @Test
    public void testCallBuilderC002() {
        logger.info("CallBuilder C002: valid sell test message");
        try {
            // <editor-fold defaultstate="collapsed" desc="Order-SELL">
            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "ORDER");
            msg.setText("{\"client\":\"101\",\"product\":\"ABC\",\"side\":\"SELL\",\"details\":{\"amount\":10,\"price\":140},\"messageType\":\"ORDER\",\"timestamp\":5,\"id\":2,\"broker\":\"3\"}");
            OrderCall order = OrderCall.CallBuilder(msg);

            assertTrue(order.getClass().isAssignableFrom(OrderCallPut.class));
            OrderCallPut putOrder = (OrderCallPut) order;
            assertEquals("101", putOrder.getClient());
            assertEquals("ABC", putOrder.getProduct());
            assertEquals(OrderSide.SELL, putOrder.getSide());
            assertEquals(10, putOrder.getDetails().getAmount());
            assertEquals(140, putOrder.getDetails().getPrice());
            assertEquals("ORDER", putOrder.getMessageType());
            assertEquals(5, putOrder.getTimestamp());
            assertEquals(2, putOrder.getId());
            assertEquals("3", putOrder.getBroker());
            //</editor-fold>

        } catch (JMSException e) {
            fail("Test case error - fail.");
        } catch (OrderCallProcessingException e) {
            fail("Test case error - fail.");
        }
    }

    /**
     * Test of CallBuilder method, of class OrderCall.
     */
    @Test
    public void testCallBuilderC003() {
        logger.info("CallBuilder C003: valid modification test message");
        try {
            // <editor-fold defaultstate="collapsed" desc="Order-MODIFICATION">
            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "MODIFICATION");
            msg.setText("{\"modifiedOrderId\":3,\"details\":{\"amount\":500,\"price\":85},\"messageType\":\"MODIFICATION\",\"timestamp\":4,\"id\":14,\"broker\":\"1\"}");
            OrderCall order = OrderCall.CallBuilder(msg);

            assertTrue(order.getClass().isAssignableFrom(OrderCallModify.class));
            OrderCallModify modOrder = (OrderCallModify) order;
            assertEquals("MODIFICATION", modOrder.getMessageType());
            assertEquals(500, modOrder.getDetails().getAmount());
            assertEquals(85, modOrder.getDetails().getPrice());
            assertEquals(4, modOrder.getTimestamp());
            assertEquals(14, modOrder.getId());
            assertEquals("1", modOrder.getBroker());
            assertEquals(3, modOrder.getModifiedOrderId());
            //</editor-fold>
        } catch (JMSException e) {
            fail("Test case error - fail.");
        } catch (OrderCallProcessingException e) {
            fail("Test case error - fail.");
        }
    }

    /**
     * Test of CallBuilder method, of class OrderCall.
     */
    @Test
    public void testCallBuilderC004() {
        logger.info("CallBuilder C004: valid cancel test message");
        try {
            // <editor-fold defaultstate="collapsed" desc="Order-CANCEL">
            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "CANCEL");
            msg.setText("{\"cancelledOrderId\":12,\"messageType\":\"CANCEL\",\"timestamp\":4,\"id\":14,\"broker\":\"2\"}");
            OrderCall order = OrderCall.CallBuilder(msg);

            assertTrue(order.getClass().isAssignableFrom(OrderCallCancel.class));
            OrderCallCancel cancelOrder = (OrderCallCancel) order;
            assertEquals("CANCEL", cancelOrder.getMessageType());
            assertEquals(4, cancelOrder.getTimestamp());
            assertEquals(14, cancelOrder.getId());
            assertEquals("2", cancelOrder.getBroker());
            assertEquals(12, cancelOrder.getCancelledOrderId());
            //</editor-fold>
        } catch (JMSException e) {
            fail("Test case error - fail.");
        } catch (OrderCallProcessingException e) {
            fail("Test case error - fail.");
        }
    }

    /**
     * Test of CallBuilder method, of class OrderCall.
     */
    @Test
    public void testCallBuilderC005() {
        logger.info("CallBuilder C005: invalid test message");
        try {
            // <editor-fold defaultstate="collapsed" desc="Invalid message text">
            TextMessage msg = new ActiveMQTextMessage();
            msg.setText("{ABC:DKE}");
            try {
                OrderCall order = OrderCall.CallBuilder(msg);
                fail("OrderCall builder accept invalid test of the message.");
            } catch (OrderCallProcessingException e) {
                assertNotEquals("", e.getMessage());
                assertEquals(null, e.getContext());
                assertEquals(null, e.getOrder());
            }
            //</editor-fold>
        } catch (JMSException e) {
            fail("Test case error - fail.");
        }
    }

}
