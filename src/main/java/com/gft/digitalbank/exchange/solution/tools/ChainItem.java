package com.gft.digitalbank.exchange.solution.tools;

/**
 * This class allows to build a chain of elements with simple and effective
 * functionality. Chain item can have a leading element and following element.
 * First element in a chain is the chain head, last element in chain is the
 * chain tail. To use a chain of elements you need a chain owner
 * {@link ChainOwner} that keeps reference (at least) to chain head. Each chain
 * item know if it is a head or a tail, its leading and following element and
 * has a reference to chain owner. This solution gives a fast iteration in both
 * directions.
 *
 * @author Marcin Buciora (c) 2016
 */
public class ChainItem {

    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Field keeps a reference to chain owner ({@link ChainOwner})
     */
    protected ChainOwner chainOwner;
    /**
     * Field keeps id of the chain. You can use id if there is no other way in
     * subclass to distinct two or more chains by chain owner. Chain id is
     * propagated to items during chain operations.
     */
    protected int chainId;
    /**
     * Field keeps a reference to leading element
     */
    protected ChainItem prevItem;
    /**
     * Field keeps a reference to following element
     */
    protected ChainItem nextItem;
    /**
     * Field keeps flag of chain head
     */
    protected Boolean isChainHead;
    /**
     * Field keep flag of chain tail
     */
    protected Boolean isChainTail;
    //</editor-fold>

    /**
     * Object constructor set default values to fields.
     */
    public ChainItem() {
        this.chainOwner = null;
        this.isChainHead = true;
        this.isChainTail = true;
        this.prevItem = null;
        this.nextItem = null;
    }

    // <editor-fold defaultstate="collapsed" desc="Chain model logic">
    /**
     * Getter, returns flag of chain head.
     *
     * @return return true if element is chain head otherwise it returns false
     */
    public Boolean isChainHead() {
        return isChainHead;
    }

    /**
     * Getter, returns flag of chain tail.
     *
     * @return return true if element is chain tail otherwise it returns false
     */
    public Boolean isChainTail() {
        return isChainTail;
    }

    /**
     * Method set a leading element to current one. If the element has already
     * any leading element a new one is inserted in the middle. Current element
     * chain owner ({@link ChainOwner}) is copied to attached element.
     * Chain element attributes are evaluated at the end ({@link evaluate}).
     *
     * @param chainItem reference to new leading item
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void setPrevItem(ChainItem chainItem) throws ChainException {
        // check special cases
        if (chainItem == this.prevItem) {
            throw new ChainException("Chain item already set as leading item.", this, this.chainOwner);
        }
        if (chainItem == this.nextItem) {
            throw new ChainException("Chain item already set as following item. Loop detected.", this, this.chainOwner);
        }
        if (chainItem == this) {
            throw new ChainException("Chain item cannot be lead by itself.", this, this.chainOwner);
        }
        if (chainItem.nextItem != null || chainItem.prevItem != null) {
             throw new ChainException("Two chains cannot be merged.", this, this.chainOwner);
        }

        chainItem.prevItem = this.prevItem;
        chainItem.nextItem = this;
        chainItem.chainOwner = this.chainOwner;
        chainItem.chainId = this.chainId;
        chainItem.evaluate();

        if (this.prevItem != null) {
            this.prevItem.nextItem = chainItem;
            this.prevItem.evaluate();
        }

        this.prevItem = chainItem;
        evaluate();
    }

    /**
     * Set a reference to chain owner. All the elements of chain have to know
     * its owner [{@link ChainOwner].
     * Use that method only for first element of the chain. All the others get its owner
     * from the element to which they are attached. At the end chain item attributes are
     * evaluated [{@link evaluate}].
     *
     * @param owner reference to chain owner object
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void setChainOwner(ChainOwner owner) throws ChainException {
        if (null == this.nextItem && null == this.prevItem) {
            this.chainOwner = owner;
            evaluate();
        }
    }

    /**
     * Attaches a following element to current one. If there is already a
     * following element the new one is put in a middle. Current element chain
     * owner [{@link ChainOwner}] is copied to attached element.
     * At the end chain item attributes are evaluated [{@link evaluate}].
     *
     * @param chainItem reference to new following element
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void setNextItem(ChainItem chainItem) throws ChainException {
        // kontrola warunków brzegowych
        if (chainItem == this.prevItem) {
            throw new ChainException("Chain item already set as following item.", this, this.chainOwner);
        }
        if (chainItem == this.nextItem) {
            throw new ChainException("Chain item already set as following item. Loop detected.", this, this.chainOwner);
        }
        if (chainItem == this) {
            throw new ChainException("Chain item cannot be followed by itself.", this, this.chainOwner);
        }
        if (chainItem.nextItem != null || chainItem.prevItem != null) {
            throw new ChainException("Two chains cannot be merged.", this, this.chainOwner);
        }

        chainItem.nextItem = this.nextItem;
        chainItem.prevItem = this;
        chainItem.chainOwner = this.chainOwner;
        chainItem.chainId = this.chainId;
        chainItem.evaluate();

        if (this.nextItem != null) {
            this.nextItem.prevItem = chainItem;
            this.nextItem.evaluate();
        }

        this.nextItem = chainItem;
        evaluate();
    }

    /**
     * Evaluation of chain item attributes. It setup head and tail flags and if
     * there is a need it notify chain owner [{@link ChainOwner] about changes.
     */
    private void evaluate() throws ChainException {
        if (null == this.prevItem) {
            this.isChainHead = true;
            if (null != this.chainOwner) {
                this.chainOwner.setChainHead(this);
            }
        } else {
            this.isChainHead = false;
        }
        if (null == this.nextItem) {
            this.isChainTail = true;
            if (null != this.chainOwner) {
                this.chainOwner.setChainTail(this);
            }
        } else {
            this.isChainTail = false;
        }
    }

    /**
     * Method allow to chain item exclude itself from the chain. If current
     * element is the last element of the chain, the chain owner
     * [{@link ChainOwner}] is notified about need of chain clearance.
     * @throws com.gft.digitalbank.exchange.solution.tools.ChainException
     */
    public void excludeFromChain() throws ChainException {
        if (this.prevItem != null) {
            this.prevItem.nextItem = this.nextItem;
            this.prevItem.evaluate();
        }
        if (nextItem != null) {
            this.nextItem.prevItem = this.prevItem;
            this.nextItem.evaluate();
        }
        if (null == prevItem && null == nextItem && null != this.chainOwner) {
            if (true == isChainHead) {
                this.chainOwner.clearChain(this.chainId);
            }
        }

        prevItem = null;
        nextItem = null;
        chainOwner = null;
        chainId = 0;
        isChainHead = false;
        isChainTail = false;
    }

    /**
     * Returns following item of the current one.
     *
     * @return following item
     */
    public ChainItem getNextItem() {
        return this.nextItem;
    }

    /**
     * Returns leading item of the current one.
     *
     * @return leading item
     */
    public ChainItem getPrevItem() {
        return this.prevItem;
    }

    /**
     * @return Returns chain id.
     */
    public int getChainId() {
        return chainId;
    }

    /**
     * Set chain id successfully only if it is a single item.
     *
     * @param chainId id of chain
     */
    public void setChainId(int chainId) {
        if (null == this.prevItem && null == this.nextItem) {
            this.chainId = chainId;
        }
    }
    //</editor-fold>

}
