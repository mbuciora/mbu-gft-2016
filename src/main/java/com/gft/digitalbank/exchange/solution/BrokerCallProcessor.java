package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.solution.engine.OrderCall;
import com.gft.digitalbank.exchange.solution.engine.OrderCallProcessingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents a message processor. It is responsible for decoding and
 * forwarding orders to asset ladders queues managed by wrappers. Processors
 * operates as thereads.
 *
 * @author Marcin Buciora (c) 2016
 * @see StockExchange
 * @see AssetProcessWrapper
 * @see AssetLadder
 */
public class BrokerCallProcessor extends Thread {

    //<editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Collection of all orders received by processor.
     */
    private final Map<Integer, OrderCall> orderCalls;
    /**
     * JMS session which is used by processor.
     */
    private final Session session;
    /**
     * JMS consumer which is used by processor.
     */
    private final MessageConsumer consumer;
    /**
     * Collection of all listeners interested in processor job's end event.
     * StockExchange watch all the processors to learn when it can get
     * transaction results.
     */
    private final List<CallProcessedEventListener> eventListeners;
    /**
     * Reference to asset manager where processor can get a queue to feed orders
     * to right asset ladder wrapped in specific thread.
     */
    private final AssetContainer assets;
    /**
     * An application context key. It allow to IoC work in multi-threaded
     * universe.
     */
    private final UUID applicationContext;
    /**
     * Global logger.
     */
    static Logger logger = LoggerFactory.getLogger(StockExchange.class);
    // </editor-fold>

    public BrokerCallProcessor(UUID applicationContext, String dstName) throws JMSException {
        this.applicationContext = applicationContext;
        this.orderCalls = new HashMap<>();
        this.eventListeners = new ArrayList<>();
        this.session = IoCContainer.get().getInstance(this.applicationContext, Connection.class).createSession(false, Session.AUTO_ACKNOWLEDGE);
        Queue queue = this.session.createQueue(dstName);
        this.consumer = this.session.createConsumer(queue);
        this.assets = IoCContainer.get().getInstance(this.applicationContext, AssetContainer.class);
    }

    //<editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Returns an order from collection according to received id.
     *
     * @param id id of the order
     * @return order for specific id
     */
    public OrderCall getOrderCall(int id) {
        return orderCalls.get(id);
    }

    /**
     * Add received order to collection.
     *
     * @param call received order call
     */
    public void registerSubsequentOrderCall(OrderCall call) {
        this.orderCalls.put(call.getId(), call);
    }

    /**
     * Remove specific order from collection.
     *
     * @param id id of order call to remove
     */
    public void unregisterOrderCall(int id) {
        this.orderCalls.remove(id);
    }

    /**
     * Thread main flow. Here processor reads data from message queue, creates
     * order calls and feeds them to specific asset leader (by a wrapper). After
     * shut down signal resources are closed and processor listeners are
     * notified that its job has been finised.
     */
    @Override
    public void run() {

        try {
            Boolean dowork = true;
            while (true == dowork) {
                javax.jms.Message message = this.consumer.receive();
                if (null != message) {
                    if (message instanceof TextMessage) {
                        TextMessage textMessage = (TextMessage) message;

                        //check if it is all
                        if (false == textMessage.getStringProperty("messageType").equals("SHUTDOWN_NOTIFICATION")) {
                            OrderCall call = OrderCall.CallBuilder(textMessage);
                            call.SetupReferences(this);
                            this.orderCalls.put(call.getId(), call);

                            // wait for your turn, sync as requested
                            while (assets.getClock() != call.getTimestamp()) {
                                try {
                                    Thread.sleep(1);
                                } catch (Exception ex) {
                                }
                            }
                            assets.getAssetQueue(call.getAssetName()).add(call);
                            assets.moveClock();
                        } else {
                            dowork = false;
                        }
                    }
                }
            }
            this.consumer.close();
            this.session.close();

            messageProcessed();
        } catch (JMSException | OrderCallProcessingException e) {
            logger.error("Processor error",e);
            // finish job
            messageProcessed();
        }
    }

    /**
     * Add new listener to collection.
     *
     * @param listener new listener
     */
    public synchronized void addListener(CallProcessedEventListener listener) {
        this.eventListeners.add(listener);
    }

    /**
     * Remove a listener from collection.
     *
     * @param listener listener to remove
     */
    public synchronized void removeListener(CallProcessedEventListener listener) {
        this.eventListeners.remove(listener);
    }

    /**
     * When job is done notify all registered listeners.
     */
    private void messageProcessed() {
        CallProcessedEvent event = new CallProcessedEvent(this);
        this.eventListeners.stream().forEach((listener) -> {
            listener.callProcessedEventReceived(event);
        });
        this.eventListeners.clear();
    }
    //</editor-fold>
}
