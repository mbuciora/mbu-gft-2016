package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;

/**
 * Represent cancel order. It is a subclass of OrderCall.
 * @see OrderCall
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallCancel extends OrderCall {

    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Canceled order id.
    */
    private int cancelledOrderId;
    /**
     * Reference to canceled order. This reference is populated when processor receives cancel order.
    */
    private OrderCallPut referenceOrder;
// </editor-fold>

    public OrderCallCancel() {
    }

    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Setup references in cancel order. Do as base class want and get reference to order mean to be
     * canceled.
     * @param processor 
     */
    @Override
    public void SetupReferences(BrokerCallProcessor processor) {
        getContext().setProcessor(processor);
        referenceOrder = (OrderCallPut) getContext().getProcessor().getOrderCall(this.cancelledOrderId);
    }

    /**
     * Get name of asset for cancel order. Take it from canceled order reference.
     * @return 
     */
    @Override
    public String getAssetName() {
        return this.referenceOrder.getAssetName();
    }

    /**
     * Execute processing for this order. Get offer conected to canceled order and run cancel action.
     * @param ladder
     * @throws Exception 
     */
    @Override
    public void Process(AssetLadder ladder) throws Exception {
        if (OrderCallStatus.BOOKED == referenceOrder.getStatus()) {
            AssetOffer offer = referenceOrder.getContext().getOffer();
            offer.cancelOrderCall(referenceOrder, this);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters/getters for class fields">
    /**
     * Getter for cancelled order id.
     * @return the cancelledOrderId
     */
    public int getCancelledOrderId() {
        return cancelledOrderId;
    }

    /**
     * Setter for cancelled order id.
     * @param cancelledOrderId the cancelledOrderId to set
     */
    public void setCancelledOrderId(int cancelledOrderId) {
        this.cancelledOrderId = cancelledOrderId;
    }

    /**
     * Getter for cancelled order reference.
     * @return the referenceOrder
     */
    public OrderCall getReferenceOrder() {
        return referenceOrder;
    }
    // </editor-fold>
}
