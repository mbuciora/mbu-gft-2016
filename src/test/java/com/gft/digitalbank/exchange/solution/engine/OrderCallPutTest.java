package com.gft.digitalbank.exchange.solution.engine;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c)
 */
public class OrderCallPutTest {

    static Logger logger = LoggerFactory.getLogger(OrderCallPutTest.class);
    private OrderCallProvider provider;

    public OrderCallPutTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        provider = new OrderCallProvider();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of consumeAmount method, of class OrderCallPut.
     */
    @Test
    public void testConsumeAmount() throws Exception {
        logger.info("consumeAmount");

        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "A", "C1", "B1");
        order.consumeAmount(50);
        assertEquals(50, order.getDetails().getCurrentAmount());
    }

    /**
     * Test of getSide method, of class OrderCallPut.
     */
    @Test
    public void testGetSide() {
        logger.info("getSide");
        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "A", "C1", "B1");
        assertEquals(OrderSide.SELL, order.getSide());
    }

    /**
     * Test of getClient method, of class OrderCallPut.
     */
    @Test
    public void testGetClient() {
        logger.info("getClient");
        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "A", "C1", "B1");
        assertEquals("C1", order.getClient());
    }

    /**
     * Test of getProduct method, of class OrderCallPut.
     */
    @Test
    public void testGetProduct() {
        logger.info("getProduct");
        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "A", "C1", "B1");
        assertEquals("A", order.getProduct());
    }

    /**
     * Test of getDetails method, of class OrderCallPut.
     */
    @Test
    public void testGetDetails() {
        logger.info("getDetails");
        OrderCallPut order = this.provider.GenerateOrderCallPut(100, 10, OrderSide.SELL, "A", "C1", "B1");
        OrderDetail details = order.getDetails();
        assertEquals(100, details.getAmount());
        assertEquals(10, details.getPrice());
        assertEquals(100, details.getCurrentAmount());
    }

    /**
     * Test of getSourceRequest method, of class OrderCallPut.
     */
    @Test
    public void testGetSourceRequest() {
        logger.info("getSourceRequest");

        OrderCallPut order1 = this.provider.GenerateOrderCallPut(150, 20, OrderSide.SELL, "A", "C1", "B1");
        OrderCallModify order2 = this.provider.GenerateOrderCallModify(order1.getId(), 100, 10, "B1");
        order1.setSourceRequest(order2);

        assertEquals(order2, order1.getSourceRequest());
    }

    /**
     * Test of setSourceRequest method, of class OrderCallPut.
     */
    @Test
    public void testSetSourceRequest() {
        logger.info("setSourceRequest");

        OrderCallPut order1 = this.provider.GenerateOrderCallPut(150, 20, OrderSide.SELL, "A", "C1", "B1");
        OrderCallModify order2 = this.provider.GenerateOrderCallModify(order1.getId(), 100, 10, "B1");
        order1.setSourceRequest(order2);

        assertEquals(order2, order1.getSourceRequest());
    }

}
