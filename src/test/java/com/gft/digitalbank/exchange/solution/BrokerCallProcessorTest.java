package com.gft.digitalbank.exchange.solution;

import java.util.UUID;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import org.mockito.Mockito;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

/**
 * @author Marcin Buciora (c) 2016
 */
public class BrokerCallProcessorTest {

    private UUID applicationContext;
    private Connection connection;
    private Session session;
    private Queue queue;
    private MessageConsumer consumer;

    public BrokerCallProcessorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        this.applicationContext = UUID.randomUUID();
        this.connection = Mockito.mock(Connection.class);
        this.session = Mockito.mock(Session.class);
        this.queue = Mockito.mock(Queue.class);
        this.consumer = Mockito.mock(MessageConsumer.class);
        
        try {
            Mockito.when(this.connection.createSession(anyBoolean(), anyInt())).thenReturn(this.session);
            Mockito.when(this.session.createQueue(anyString())).thenReturn((Queue)this.queue);
            Mockito.when(this.session.createConsumer(any())).thenReturn(this.consumer);
        } catch (JMSException ex) {
            fail("Unexpected exception.");
        }
        IoCContainer.get().registerInstance(this.applicationContext, Connection.class, this.connection);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of removeListener method, of class BrokerCallProcessor.
     */
    @Test
    public void testRemoveListener() {
        System.out.println("removeListener");

        CallProcessedEventListener listener = Mockito.mock(CallProcessedEventListener.class);
        String destinationName = "Destination001";

        try {
            BrokerCallProcessor procesor = new BrokerCallProcessor(this.applicationContext, destinationName);
            procesor.addListener(listener);
            procesor.removeListener(listener);
        } catch (Exception ex) {
            fail("Unexpected exception received.");
        }
    }

}
