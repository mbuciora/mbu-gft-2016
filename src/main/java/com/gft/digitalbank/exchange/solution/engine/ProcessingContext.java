
package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;

/**
 * Specific information about processing context. It allow order to reach processor or ladder or offer.
 * @author Marcin Buciora (c) 2016
 */
public class ProcessingContext {
    /**
     * Processor reference.
     */
    private BrokerCallProcessor processor;
    /**
     * Asset ladder reference.
     */
    private AssetLadder ladder;
    /**
     * Offer reference.
     */
    private AssetOffer offer;

    /**
     * Getter for processor reference.
     * @return the processor
     */
    public BrokerCallProcessor getProcessor() {
        return processor;
    }

    /**
     * Setter for processor reference.
     * @param processor the processor to set
     */
    public void setProcessor(BrokerCallProcessor processor) {
        this.processor = processor;
    }

    /**
     * Getter for asset ladder reference.
     * @return the ladder
     */
    public AssetLadder getLadder() {
        return ladder;
    }

    /**
     * Setter for asset ladder reference.
     * @param ladder the ladder to set
     */
    public void setLadder(AssetLadder ladder) {
        this.ladder = ladder;
    }

    /**
     * Getter for offer reference.
     * @return the offer
     */
    public AssetOffer getOffer() {
        return offer;
    }

    /**
     * Setter for offer reference.
     * @param offer the offer to set
     */
    public void setOffer(AssetOffer offer) {
        this.offer = offer;
    }
}
