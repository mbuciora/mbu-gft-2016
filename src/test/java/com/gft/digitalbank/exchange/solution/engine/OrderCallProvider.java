package com.gft.digitalbank.exchange.solution.engine;

import java.util.HashMap;
import java.util.Map;
import javax.jms.TextMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import static org.junit.Assert.fail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallProvider {
    static Logger logger = LoggerFactory.getLogger(OrderCallProvider.class);
    private long clock = 1;
    private int orderId = 1;
    private Map<Integer, OrderCall> orders;

    public OrderCallProvider() {
        this.clock = 1;
        this.orderId = 1;
        this.orders = new HashMap<Integer, OrderCall>();
    }

    public OrderCallPut GenerateOrderCallPut(int amount, int price, OrderSide side, String product, String client, String broker) {
        try {
            String content = "{\"client\":\"" + client + "\",\"product\":\"" + product + "\",\"side\":\"" + side.toString() + "\",\"details\":{\"amount\":"
                    + amount + ",\"price\":" + price + "},\"messageType\":\"ORDER\",\"timestamp\":" + this.clock + ",\"id\":" + this.orderId + ",\"broker\":\"" + broker + "\"}";

            this.clock++;
            this.orderId++;

            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "ORDER");
            msg.setText(content);
            OrderCallPut order = (OrderCallPut) OrderCall.CallBuilder(msg);

            this.orders.put(order.getId(), order);
            return order;
        } catch (Exception e) {
            fail("Cannot prepare put order!");
            return null;
        }
    }
    
    public OrderCallModify GenerateOrderCallModify(int modifiedOrderId, int amount, int price, String broker) {
        try {
            String content = "{\"modifiedOrderId\":"+modifiedOrderId+",\"details\":{\"amount\":"+amount+",\"price\":"+price+
                    "},\"messageType\":\"MODIFICATION\",\"timestamp\":"+this.clock+",\"id\":"+this.orderId+",\"broker\":\""+broker+"\"}" ;

            this.clock++;
            this.orderId++;

            TextMessage msg = new ActiveMQTextMessage();
            msg.setStringProperty("messageType", "MODIFICATION");
            msg.setText(content);
            OrderCallModify order = (OrderCallModify) OrderCall.CallBuilder(msg);

            this.orders.put(order.getId(), order);
            return order;
        } catch (Exception e) {
            fail("Cannot prepare put order!");
            return null;
        }
    }
}
