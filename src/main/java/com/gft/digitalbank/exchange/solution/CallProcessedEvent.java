package com.gft.digitalbank.exchange.solution;

import java.util.EventObject;

/**
 * Class represents an event used to notify listeners that processor has finished its job.
 * @author  Marcin Buciora (c) 2016
 */
public class CallProcessedEvent extends EventObject {
    /**
     * Processor that notify.
    */
    private final BrokerCallProcessor source;
    
    public CallProcessedEvent(BrokerCallProcessor processor) {
        super(processor);
        this.source = processor;
    }
    
    /**
     * Getter for event source reference.
     * @return object that triggered the event
     */
    public BrokerCallProcessor getSource(){
        return source;
    }
}
