package com.gft.digitalbank.exchange.solution.tools;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Chain item unit test.
 * @author Marcin Buciora (c) 2016
 */
public class ChainItemTest {

    static Logger logger = LoggerFactory.getLogger(ChainItemTest.class);

    public ChainItemTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of isChainHead method, of class ChainItem.
     */
    @Test
    public void testIsChainHeadC001() {
        logger.info("isChainHead C001: Single item");
        ChainItem instance = new ChainItem();
        Boolean expResult = true;
        Boolean result = instance.isChainHead();
        assertEquals(expResult, result);
    }

    /**
     * Test of isChainHead method, of class ChainItem.
     */
    @Test
    public void testIsChainHeadC002() {
        logger.info("isChainHead C002: Two items, set prev");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci2.setPrevItem(ci1);

            assertEquals(true, ci1.isChainHead());
            assertEquals(false, ci2.isChainHead());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of isChainHead method, of class ChainItem.
     */
    @Test
    public void testIsChainHeadC003() {
        logger.info("isChainHead C003: Three items, set prev in the middle");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci3.setPrevItem(ci1);

            assertEquals(true, ci1.isChainHead());

            ci3.setPrevItem(ci2);

            assertEquals(true, ci1.isChainHead());
            assertEquals(false, ci2.isChainHead());
            assertEquals(false, ci3.isChainHead());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of isChainTail method, of class ChainItem.
     */
    @Test
    public void testIsChainTailC001() {
        logger.info("isChainTail C001: Single item");
        ChainItem instance = new ChainItem();
        Boolean expResult = true;
        Boolean result = instance.isChainTail();
        assertEquals(expResult, result);
    }

    /**
     * Test of isChainTail method, of class ChainItem.
     */
    @Test
    public void testIsChainTailC002() {
        logger.info("isChainTail C002: Two items, set next");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);

            assertEquals(true, ci2.isChainTail());
            assertEquals(false, ci1.isChainTail());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of isChainTail method, of class ChainItem.
     */
    @Test
    public void testIsChainTailC003() {
        logger.info("isChainTail C003: Three items, set next in the middle");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci1.setNextItem(ci3);

            assertEquals(true, ci3.isChainTail());

            ci1.setNextItem(ci2);

            assertEquals(true, ci3.isChainTail());
            assertEquals(false, ci2.isChainTail());
            assertEquals(false, ci1.isChainTail());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setPrevItem method, of class ChainItem.
     */
    @Test
    public void testSetPrevItemC001() {
        logger.info("setPrevItem C001: Two items direct");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci2.setPrevItem(ci1);
            assertEquals(ci1, ci2.getPrevItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setPrevItem method, of class ChainItem.
     */
    @Test
    public void testSetPrevItemC002() {
        logger.info("setPrevItem C002: Special cases");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci3.setPrevItem(ci2);
            assertEquals(ci2, ci3.getPrevItem());
            ci2.setPrevItem(ci1);
            assertEquals(ci1, ci2.getPrevItem());

            try {
                ci2.setPrevItem(ci1);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci1, ci2.getPrevItem());
            }
            try {
                ci1.setPrevItem(ci2);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(null, ci1.getPrevItem());
            }
            try {
                ci2.setPrevItem(ci2);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci1, ci2.getPrevItem());
            }

            ChainItem ci4 = new ChainItem();
            ChainItem ci5 = new ChainItem();
            ChainItem ci6 = new ChainItem();
            ci4.setNextItem(ci5);

            try {
                ci2.setPrevItem(ci4);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                ChainItem cix = e.getChainItem();
                assertEquals(ci2, cix);
                assertEquals(ci1, ci2.getPrevItem());
            }
            try {
                ci2.setPrevItem(ci5);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci1, ci2.getPrevItem());
            }
            ci5.setPrevItem(ci6);
            try {
                ci2.setNextItem(ci5);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci1, ci2.getPrevItem());
            }
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setChainOwner method, of class ChainItem.
     */
    @Test
    public void testSetChainOwnerC001() {
        logger.info("setChainOwner C001: Single item");
        try {
            ChainOwnerMock owner = new ChainOwnerMock();
            ChainItem ci1 = new ChainItem();
            ci1.setChainOwner(owner);
            ci1.setChainId(1);
            owner.setChainHead(ci1);

            assertEquals(owner, ci1.chainOwner);
            assertEquals(ci1, owner.getFirstChainHead());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setNextItem method, of class ChainItem.
     */
    @Test
    public void testSetNextItemC001() {
        logger.info("setNextItem C001: Two items direct");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);
            assertEquals(ci2, ci1.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setNextItem method, of class ChainItem.
     */
    @Test
    public void testSetNextItemC002() {
        logger.info("setNextItem C002: Three items direct");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci1.setNextItem(ci2);
            assertEquals(ci2, ci1.getNextItem());
            ci2.setNextItem(ci3);
            assertEquals(ci3, ci2.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setNextItem method, of class ChainItem.
     */
    @Test
    public void testSetNextItemC003() {
        logger.info("setNextItem C003: Three items in the middle");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci1.setNextItem(ci3);
            assertEquals(ci3, ci1.getNextItem());
            ci1.setNextItem(ci2);
            assertEquals(ci2, ci1.getNextItem());
            assertEquals(ci3, ci2.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of setNextItem method, of class ChainItem.
     */
    @Test
    public void testSetNextItemC004() {
        logger.info("setNextItem C004: Special cases");
        try {
            ChainOwner owner = new ChainOwnerMock();
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ChainItem ci3 = new ChainItem();
            ci1.setChainOwner(owner);
            ci1.setNextItem(ci2);
            ci2.setNextItem(ci3);
            assertEquals(ci2, ci1.getNextItem());
            assertEquals(ci3, ci2.getNextItem());

            try {
                ci1.setNextItem(ci2);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci2, ci1.getNextItem());
                assertEquals(e.getChainOwner(), owner);
                assertEquals(e.getChainOwner(), ci2.chainOwner);
                assertEquals(e.getChainOwner(), ci3.chainOwner);
            }
            try {
                ci2.setNextItem(ci1);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci3, ci2.getNextItem());
                assertEquals(e.getChainOwner(), owner);
                assertEquals(e.getChainOwner(), ci2.chainOwner);
                assertEquals(e.getChainOwner(), ci3.chainOwner);
            }
            try {
                ci2.setNextItem(ci2);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci3, ci2.getNextItem());
                assertEquals(e.getChainOwner(), owner);
                assertEquals(e.getChainOwner(), ci2.chainOwner);
                assertEquals(e.getChainOwner(), ci3.chainOwner);
            }

            ChainItem ci4 = new ChainItem();
            ChainItem ci5 = new ChainItem();
            ChainItem ci6 = new ChainItem();
            ci4.setNextItem(ci5);

            try {
                ci2.setNextItem(ci4);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci3, ci2.getNextItem());
            }
            try {
                ci2.setNextItem(ci5);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci3, ci2.getNextItem());
            }
            ci5.setNextItem(ci6);
            try {
                ci2.setNextItem(ci5);
                fail("failed to keep consistency of chain");
            } catch (ChainException e) {
                assertEquals(ci3, ci2.getNextItem());
            }

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of excludeFromChain method, of class ChainItem.
     */
    @Test
    public void testExcludeFromChainC001() {
        logger.info("excludeFromChain C001: Single item");
        try {
            ChainOwnerMock owner = new ChainOwnerMock();
            ChainItem ci1 = new ChainItem();
            ci1.setChainOwner(owner);
            ci1.setChainId(1);
            owner.setChainHead(ci1);

            assertEquals(owner, ci1.chainOwner);
            assertEquals(ci1, owner.getFirstChainHead());

            ci1.excludeFromChain();

            assertEquals(null, owner.getFirstChainHead());
            assertEquals(null, owner.getFirstChainTail());

            assertEquals(null, ci1.chainOwner);
            assertEquals(false, ci1.isChainHead());
            assertEquals(false, ci1.isChainTail());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of excludeFromChain method, of class ChainItem.
     */
    @Test
    public void testExcludeFromChainC002() {
        logger.info("excludeFromChain C002: two items");
        try {
            ChainOwnerMock owner = new ChainOwnerMock();
            ChainItem ci1 = new ChainItem();
            ci1.setChainOwner(owner);
            ci1.setChainId(1);
            owner.setChainHead(ci1);

            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);

            ci1.excludeFromChain();

            assertEquals(true, ci2.isChainHead());
            assertEquals(true, ci2.isChainTail());

            assertEquals(null, ci2.getPrevItem());
            assertEquals(null, ci2.getNextItem());

            assertEquals(null, ci1.chainOwner);
            assertEquals(false, ci1.isChainHead());
            assertEquals(false, ci1.isChainTail());
            assertEquals(null, ci1.getNextItem());
            assertEquals(null, ci1.getPrevItem());

        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of excludeFromChain method, of class ChainItem.
     */
    @Test
    public void testExcludeFromChainC003() {
        logger.info("excludeFromChain C003: three items");
        try {
            ChainOwnerMock owner = new ChainOwnerMock();
            ChainItem ci1 = new ChainItem();
            ci1.setChainOwner(owner);
            ci1.setChainId(1);
            owner.setChainHead(ci1);

            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);

            ChainItem ci3 = new ChainItem();
            ci2.setNextItem(ci3);

            ci2.excludeFromChain();

            assertEquals(true, ci1.isChainHead());
            assertEquals(true, ci3.isChainTail());
            assertEquals(ci3, ci1.getNextItem());
            assertEquals(null, ci1.getPrevItem());
            assertEquals(null, ci3.getNextItem());
            assertEquals(ci1, ci3.getPrevItem());

            assertEquals(null, ci2.getPrevItem());
            assertEquals(null, ci2.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of getNextItem method, of class ChainItem.
     */
    @Test
    public void testGetNextItemC001() {
        logger.info("getNextItem C001: Two items direct");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);
            assertEquals(ci2, ci1.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of getPrevItem method, of class ChainItem.
     */
    @Test
    public void testGetPrevItemC001() {
        logger.info("getPrevItem C001: Two items direct");
        try {
            ChainItem ci1 = new ChainItem();
            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);
            assertEquals(ci2, ci1.getNextItem());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     * Test of getChainId method, of class ChainItem.
     */
    @Test
    public void testGetChainIdC001() {
        logger.info("getChainId C001: Single item");
        ChainItem instance = new ChainItem();
        int expResult = 101;
        instance.setChainId(expResult);
        int result = instance.getChainId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setChainId method, of class ChainItem.
     */
    @Test
    public void testSetChainIdC001() {
        logger.info("setChainId C001: Single item");
        ChainItem instance = new ChainItem();
        int expResult = 101;
        instance.setChainId(expResult);
        int result = instance.getChainId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setChainId method, of class ChainItem.
     */
    @Test
    public void testSetChainIdC002() {
        logger.info("setChainId C002: two items");
        try {
            ChainItem ci1 = new ChainItem();
            int chainId = 101;
            ci1.setChainId(chainId);
            ChainItem ci2 = new ChainItem();
            ci1.setNextItem(ci2);
            assertEquals(chainId, ci2.getChainId());
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     *
     */
    @Test
    public void performanceBuildLongChain() {
        logger.info("performanceBuildLongChain C001: 10.001 items");
        try {
            ChainItem head = new ChainItem();
            ChainItem current = head;
            for (int i = 0; i < 100000; i++) {
                ChainItem item = new ChainItem();
                current.setNextItem(item);
                current = item;
            }

            int size = 0;
            current = head;
            while (null != current) {
                size++;
                current = current.getNextItem();
            }
            assertEquals(100001, size);
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }

    /**
     *
     */
    @Test
    public void performanceBuildAndDestroyingLongChain() {
        logger.info("performanceBuildAndDestroyingLongChain C001: 100.001 items");
        try {
            ChainItem head = new ChainItem();
            ChainItem current = head;
            for (int i = 0; i < 100000; i++) {
                ChainItem item = new ChainItem();
                current.setNextItem(item);
                current = item;
            }

            int size = 0;
            ChainItem prev = null;
            while (current != head) {
                prev = current.getPrevItem();
                current.excludeFromChain();
                size++;
                current = prev;
            }

            assertEquals(100000, size);
        } catch (Exception e) {
            fail("Exception found: " + e.getMessage());
        }
    }
}
