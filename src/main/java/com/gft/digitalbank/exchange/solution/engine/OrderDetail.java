
package com.gft.digitalbank.exchange.solution.engine;

/**
 * OrderDetails keeps amount and price of put order or order modification.
 * It exposes method for decreasing available amount of order call as well as a getter for 
 * amount currently available for transaction..
 * @see OrderCallPut
 * @see OrderCallModify
 * @author Marcin Buciora (c) 2016
 * @version 1.0.0
 */
public class OrderDetail {

    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
    * Initial amount of the order.
    */
    private int amount;
    /**
     * Price required by the client
     */
    private int price;
    /**
     * Amount currently available for transactions
     */
    private int currentAmount;
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Reset current amount to start amount ot the order call.
     */
    public void resetCurrentAmount() {
        this.currentAmount = this.amount;
    }
    
    /**
     * When sell and buy orders meet, then certain amount is consumed on both sides.
     * This methods decreases amount available for future transactions.
     * @param val It is amount consumed by current transaction.
     */
    public void consumeAmount(int val) throws IllegalArgumentException {
        if ( val > this.currentAmount || 0 >= val) throw new IllegalArgumentException("Invalid amount to consume. Requested: "+val +", available: " + this.currentAmount);
        this.currentAmount -= val;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters\getters for class fields">
    /**
     * Getter for initial amount field.
     * @return the initial amount of the order
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Setter for initial amount of the order. Reset currentAmount field.
     * @param amount the initial amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
        this.currentAmount = amount;
    }

    /**
     * Getter for required price.
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * Setter for required price of the order.
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * Getter for currently available amount of the order
     * @return the currentAmount
     */
    public int getCurrentAmount() {
        return currentAmount;
    }
    // </editor-fold>
}
