
package com.gft.digitalbank.exchange.solution.engine;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;
import com.gft.digitalbank.exchange.solution.tools.ChainItem;
import com.google.gson.Gson;
import java.io.IOException;
import javax.jms.JMSException;
import javax.jms.TextMessage;

/**
 * General implementation of order call representation. Is base class for all kind of orders.
 * @author Marcin Buciora (c) 2016
 */
public abstract class OrderCall extends ChainItem {
    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Source message type as string
     */
    private String messageType;
    /**
     * Order id
     */
    private int id;
    /**
     * Order timestamp
     */
    private long timestamp;
    /**
     * Name of broker which sent this order
     */
    private String broker;
    /**
     * Reference to processing context.
     */
    private final ProcessingContext context;
    /**
     * Order status.
     */
    private OrderCallStatus status;
    // </editor-fold>

    public OrderCall() {
        this.context = new ProcessingContext();
        this.status = OrderCallStatus.NEW;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Builder method for getting an instance of order call from text message.
     * @param msg
     * @return
     * @throws OrderCallProcessingException
     * @throws JMSException 
     */
    public static OrderCall CallBuilder(TextMessage msg) throws OrderCallProcessingException, JMSException {
        Gson gson = new Gson();
        OrderCall call = null;
        
        try {
            String msgty = msg.getStringProperty("messageType");
            if ( null == msgty ) throw new OrderCallProcessingException("Invalid text message content: "+msg.getText(), null, null, null);
            switch(msgty){
                case "ORDER":
                    call = gson.fromJson(msg.getText(), OrderCallPut.class);
                    break;
                case "MODIFICATION":
                    call = gson.fromJson(msg.getText(), OrderCallModify.class);
                    break;
                case "CANCEL":
                    call = gson.fromJson(msg.getText(), OrderCallCancel.class);
                    break;
            }
            call.initialize();
        } catch (JMSException e) {
            throw new OrderCallProcessingException("Cannot process text message to order call object: ["+msg.getStringProperty("messageType")+"] "+msg.getText(), e, null, null);
        }
        
        return call;
    }
    
    /**
     * We use GSON for its speed but GSON do not use setters so we need to initialize proper object state manually
     */
    protected void initialize(){}
    
    /**
     * Each order kind provide its implementation of processing flow.
     * @param ladder
     * @throws Exception 
     */
    public abstract void Process(AssetLadder ladder)  throws Exception;
    
    /**
     * Abstract method to get asset name. Especially for modification and cancel order.
     * @return 
     */
    public abstract String getAssetName();
    
    /**
     * Allow to setup references, especially for modification and cancel order.
     * @param processor 
     */
    public void SetupReferences(BrokerCallProcessor processor) {
        getContext().setProcessor(processor);
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Setters/getters for class fields">
    
    /**
     * Getter for messageType.
     * @return the messageType
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Setter for messageType.
     * @param messageType the messageType to set
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    /**
     * Getter for order id.
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter for order id.
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Getter for order timestamp.
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Setter for order timestamp.
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Getter for broker name.
     * @return the broker
     */
    public String getBroker() {
        return broker;
    }

    /**
     * Setter for broker name.
     * @param broker the broker to set
     */
    public void setBroker(String broker) {
        this.broker = broker;
    }

    /**
     * Getter for processing context.
     * @return the context
     */
    public ProcessingContext getContext() {
        return context;
    }

    /**
     * Getter for order status.
     * @return the status
     */
    public OrderCallStatus getStatus() {
        return status;
    }

    /**
     * Setter for order status.
     * @param status the status to set
     */
    public void setStatus(OrderCallStatus status) {
        this.status = status;
    }    
    // </editor-fold>
}
