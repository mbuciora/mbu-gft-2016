package com.gft.digitalbank.exchange.solution.engine;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderSideTest {
    static Logger logger = LoggerFactory.getLogger(OrderSideTest.class);

    public OrderSideTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of fromString method, of class OrderSide.
     */
    @Test
    public void testFromString() {
        logger.info("fromString");
        String name = "BUY";
        OrderSide expResult = OrderSide.BUY;
        OrderSide result = OrderSide.fromString(name);
        assertEquals(expResult, result);

        name = "SELL";
        expResult = OrderSide.SELL;
        result = OrderSide.fromString(name);
        assertEquals(expResult, result);

        name = "UNKNOWN";
        expResult = OrderSide.UNKNOWN;
        result = OrderSide.fromString(name);
        assertEquals(expResult, result);

        name = "ABC182";
        expResult = OrderSide.UNKNOWN;
        result = OrderSide.fromString(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of fromInt method, of class OrderSide.
     */
    @Test
    public void testFromInt() {
        logger.info("fromInt");
        int value = 1;
        OrderSide expResult = OrderSide.BUY;
        OrderSide result = OrderSide.fromInt(value);
        assertEquals(expResult, result);

        value = 2;
        expResult = OrderSide.SELL;
        result = OrderSide.fromInt(value);
        assertEquals(expResult, result);

        value = 0;
        expResult = OrderSide.UNKNOWN;
        result = OrderSide.fromInt(value);
        assertEquals(expResult, result);

        value = 95;
        expResult = OrderSide.UNKNOWN;
        result = OrderSide.fromInt(value);
        assertEquals(expResult, result);
    }

    /**
     * Test of toInt method, of class OrderSide.
     */
    @Test
    public void testToInt() {
        logger.info("toInt");
        OrderSide value = OrderSide.BUY;
        int expResult = 1;
        int result = OrderSide.toInt(value);
        assertEquals(expResult, result);

        value = OrderSide.SELL;
        expResult = 2;
        result = OrderSide.toInt(value);
        assertEquals(expResult, result);

        value = OrderSide.UNKNOWN;
        expResult = 0;
        result = OrderSide.toInt(value);
        assertEquals(expResult, result);
    }

}
