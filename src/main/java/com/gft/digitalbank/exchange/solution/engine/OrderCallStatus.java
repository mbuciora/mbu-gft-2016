
package com.gft.digitalbank.exchange.solution.engine;

/**
 * Order call flow througth several states. This enum named them.
 * @author Marcin Buciora (c) 2016
 */
public enum OrderCallStatus {
    /**
     * New order call waiting for processing.
     */
    NEW,
    /**
     * Order became a part of offer in an asset ladder.
     */
    BOOKED,
    /**
     * Order been cancelled.
     */
    CANCELL,
    /**
     * Order evolved into transaction(s).
     */
    EXECUTED
}
