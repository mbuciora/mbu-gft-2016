package com.gft.digitalbank.exchange.solution;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import java.util.UUID;

/**
 * Simple implementation of Inverse of Control pattern. Allow to use application
 * context key. Context is used for separation in multi-threaded environment.
 *
 * @author Marcin Buciora (c) 2016
 */
public class IoCContainer {

    //<editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Table with instance mapping.
     */
    private Table<UUID, Class<?>, Object> instances;
    /**
     * Reference to IoC container.
     */
    private static IoCContainer container;
    //</editor-fold>
    
    private IoCContainer() {
        this.instances = HashBasedTable.create();
    }

    //<editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Getter for reference IoC container.
     * @return IoC container
     */
    public static IoCContainer get() {
        if (null == IoCContainer.container) {
            IoCContainer.container = new IoCContainer();
        }
        return IoCContainer.container;
    }

    /**
     * Here you can register and instance for certain interface or class definition. There is a need for 
     * context key which separates environments.
     * @param <T> specific type for registration
     * @param applicationContext key of the context
     * @param type registered type
     * @param instance registered instance
     * @throws IllegalArgumentException It is not allowed to register null instance or null type or type that already was registered
     */
    public <T> void registerInstance(UUID applicationContext, Class<T> type, T instance) throws IllegalArgumentException {
            if (null == type || null == instance) {
                throw new IllegalArgumentException("Null reference received. Cannot register.");
            }
            if (false == this.instances.contains(applicationContext, type)) {
                this.instances.put(applicationContext, type, instance);
            } else {
                throw new IllegalArgumentException("Instance of this type already set.");
            }
    }

    /**
     * Returns instance for given context key and type.
     * @param <T> type of instance
     * @param applicationContext key of the context
     * @param type type of instance
     * @return instance registered for specific type
     */
    public <T> T getInstance(UUID applicationContext, Class<T> type) {
        if (null != this.instances.get(applicationContext, type)) {
            return type.cast(this.instances.get(applicationContext, type));
        } else {
            return null;
        }
    }

    /**
     * Clear all registered instances for specific context key
    */
    public void clean(UUID applicationContext) {
        this.instances.row(applicationContext).clear();
    }

    /**
     * Clear all registered instances.
     */
    public void clean() {
        this.instances.clear();
    }
    //</editor-fold>
}
