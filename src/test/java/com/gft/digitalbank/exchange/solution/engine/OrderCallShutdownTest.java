package com.gft.digitalbank.exchange.solution.engine;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallShutdownTest {
    static Logger logger = LoggerFactory.getLogger(OrderCallShutdownTest.class);
    
    public OrderCallShutdownTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of SetupReferences method, of class OrderCallShutdown.
     */
    @Test
    public void testSetupReferences() {
        logger.info("SetupReferences");

        OrderCallShutdown order = new OrderCallShutdown();
        order.initialize();
        try {
            order.SetupReferences(null);
            assertEquals(null, order.getContext().getProcessor());
        } catch (Exception ex){
            fail("There cannot be any exception.");
        }
        
    }

    /**
     * Test of getAssetName method, of class OrderCallShutdown.
     */
    @Test
    public void testGetAssetName() {
        logger.info("getAssetName");
        
        OrderCallShutdown order = new OrderCallShutdown();
        try {
            assertEquals(null, order.getAssetName());
        } catch (Exception ex){
            fail("There cannot be any exception.");
        }
    }

    /**
     * Test of Process method, of class OrderCallShutdown.
     */
    @Test
    public void testProcess() throws Exception {
        logger.info("Process");
        
        OrderCallShutdown order = new OrderCallShutdown();
        order.initialize();
        try {
            order.Process(null);
        } catch (Exception ex){
            fail("There cannot be any exception.");
        }
    }
    
}
