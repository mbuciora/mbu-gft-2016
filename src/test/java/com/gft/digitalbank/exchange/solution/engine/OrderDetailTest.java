package com.gft.digitalbank.exchange.solution.engine;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderDetailTest {

    static Logger logger = LoggerFactory.getLogger(OrderDetailTest.class);

    public OrderDetailTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of consumeAmount method, of class OrderDetail.
     */
    @Test
    public void testConsumeAmountC001() {
        logger.info("consumeAmount C001: simple consuming");

        OrderDetail details = new OrderDetail();
        try {
            details.setAmount(234);
            details.setPrice(123);

            details.consumeAmount(34);
            assertEquals(200, details.getCurrentAmount());

            details.consumeAmount(150);
            assertEquals(50, details.getCurrentAmount());

            details.consumeAmount(50);
            assertEquals(0, details.getCurrentAmount());
        } catch (Exception ex) {
            fail("Invalid result with cosnsumeAmount method. " + ex.getMessage());
        }
    }

    /**
     * Test of consumeAmount method, of class OrderDetail.
     */
    @Test
    public void testConsumeAmountC002() {
        logger.info("consumeAmount C002: invalid amount to consume");

        OrderDetail details = new OrderDetail();
        details.setAmount(453);
        details.setPrice(89);

        try {
            details.consumeAmount(-30);
            fail("Invalid result with cosnsumeAmount method.");
        } catch (IllegalArgumentException ex) {
            assertEquals(453, details.getCurrentAmount());
        }

        try {
            details.consumeAmount(454);
            fail("Invalid result with cosnsumeAmount method.");
        } catch (IllegalArgumentException ex) {
            assertEquals(453, details.getCurrentAmount());
        }

    }

    /**
     * Test of getAmount method, of class OrderDetail.
     */
    @Test
    public void testGetAmount() {
        logger.info("getAmount");

        OrderDetail details = new OrderDetail();
        details.setAmount(786);
        details.setPrice(987);
        assertEquals(786, details.getAmount());
    }

    /**
     * Test of setAmount method, of class OrderDetail.
     */
    @Test
    public void testSetAmount() {
        logger.info("setAmount");

        OrderDetail details = new OrderDetail();
        details.setAmount(786);
        details.setPrice(987);
        assertEquals(786, details.getAmount());

        details.setAmount(231);
        assertEquals(231, details.getAmount());
    }

    /**
     * Test of getPrice method, of class OrderDetail.
     */
    @Test
    public void testGetPrice() {
        logger.info("getPrice");

        OrderDetail details = new OrderDetail();
        details.setAmount(786);
        details.setPrice(987);
        assertEquals(987, details.getPrice());

        details.setPrice(111);
        assertEquals(111, details.getPrice());

    }

    /**
     * Test of setPrice method, of class OrderDetail.
     */
    @Test
    public void testSetPrice() {
        logger.info("setPrice");

        OrderDetail details = new OrderDetail();
        details.setAmount(120);
        details.setPrice(250);
        assertEquals(250, details.getPrice());

        details.setPrice(300);
        assertEquals(300, details.getPrice());
    }

    /**
     * Test of getCurrentAmount method, of class OrderDetail.
     */
    @Test
    public void testGetCurrentAmount() {
        logger.info("getCurrentAmount");

        OrderDetail details = new OrderDetail();
        try {
            details.setAmount(200);
            details.setPrice(100);

            details.consumeAmount(50);
            assertEquals(150, details.getCurrentAmount());

            details.consumeAmount(120);
            assertEquals(30, details.getCurrentAmount());

            details.consumeAmount(30);
            assertEquals(0, details.getCurrentAmount());
        } catch (Exception ex) {
            fail("Invalid result with cosnsumeAmount method. " + ex.getMessage());
        }
    }

}
