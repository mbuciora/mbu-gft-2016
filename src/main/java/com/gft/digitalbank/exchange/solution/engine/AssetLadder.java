package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.model.OrderBook;
import com.gft.digitalbank.exchange.model.OrderEntry;
import com.gft.digitalbank.exchange.model.Transaction;
import com.gft.digitalbank.exchange.solution.tools.ChainException;
import com.gft.digitalbank.exchange.solution.tools.ChainItem;
import com.gft.digitalbank.exchange.solution.tools.ChainOwner;
import java.util.ArrayList;
import java.util.List;

/**
 * Asset ladder represents asset data and operations caused by incoming order calls.
 * It keeps current offers (sale and buy chains) and executed transactions. Each incoming order it put
 * to chain for processing which can result with new transactions or new offer.
 * @author Marcin Buciora (c)2016
 */
public class AssetLadder implements ChainOwner {

    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Head element of sell offer chain.
     * @see AssetOffer
     */
    private AssetOffer sellOfferChainHead;
    /**
     * Tail element of sell offer chain.
     */
    private AssetOffer sellOfferChainTail;
    /**
     * Head element of buy offer chain.
     */
    private AssetOffer buyOfferChainHead;
    /**
     * Tail element of buy offer chain.
     */
    private AssetOffer buyOfferChainTail;
    /**
     * Asset name.
     */
    private final String assetName;
    /**
     * Count for transaction ids.
     */
    private int transactionCount;
    /**
     * Collection of transactions.
     */
    private final List<Transaction> transactions;
    // </editor-fold>

    public AssetLadder(String name) {
        this.assetName = name;
        this.transactions = new ArrayList<>();
    }

    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Entry point for processing a call from broker. {@link OrderCall} is base
     * class for incoming orders.
     *
     * @throws java.lang.Exception
     * @see OrderCall
     * @param call
     */
    public void processCall(OrderCall call) throws Exception {
        call.Process(this);
    }

    /**
     * Process buy or sell order call dispatcher.
     * @param call
     * @throws Exception 
     */
    public void putOrder(OrderCallPut call) throws Exception {
        switch (call.getSide()) {
            case BUY:
                putBuyOffer(call);
                break;
            case SELL:
                putSellOffer(call);
                break;
        }
    }

    /**
     * Process sell order call. If there is situation that transaction occure then it is processed. When any quantity
     * left then it is passed to the chain for incorporation.
     * @param call
     * @throws ChainException
     * @throws IllegalArgumentException 
     */
    private void putSellOffer(OrderCallPut call) throws ChainException, IllegalArgumentException {
        if (null != this.buyOfferChainHead) {
            AssetOffer currentBuy = this.buyOfferChainHead;
            // <editor-fold defaultstate="collapsed" desc="Transaction generation process">
            if (currentBuy.getPrice() >= call.getDetails().getPrice()) {
                // there is at least one potencial transaction...
                Boolean done = false;
                OrderCallPut curBuyReq = currentBuy.getOrderCallChainHead();
                while (false == done) {
                    int amount;
                    int price = currentBuy.getPrice();

                    if (call.getDetails().getCurrentAmount() >= curBuyReq.getDetails().getCurrentAmount()) {
                        amount = curBuyReq.getDetails().getCurrentAmount();
                    } else {
                        amount = call.getDetails().getCurrentAmount();
                    }

                    Transaction tr = new Transaction(
                            getTransactionNextId(),
                            amount,
                            price,
                            this.assetName,
                            curBuyReq.getBroker(),
                            call.getBroker(),
                            curBuyReq.getClient(),
                            call.getClient()
                    );
                    this.transactions.add(tr);

                    curBuyReq = curBuyReq.consumeAmount(amount);
                    call.consumeAmount(amount);

                    if (null == curBuyReq) {
                        currentBuy = null;
                    } else {
                        currentBuy = curBuyReq.getOffer();
                    }

                    // curBuyReq == null means that this asset doesn't has any offer any more
                    if (null == currentBuy) {
                        done = true;
                    } else if (currentBuy.getPrice() < call.getDetails().getPrice()) {
                        done = true;
                    }

                    // if 0 then whole order was matched
                    if (0 == call.getDetails().getCurrentAmount()) {
                        done = true;
                    }
                }
            }
            // </editor-fold>
        }
        // <editor-fold defaultstate="collapsed" desc="Extend ladder with unconsumer order part">
        // if there is an unconsumed amount then we add it to the ladder
        if (0 < call.getDetails().getCurrentAmount()) {
            if (null == this.sellOfferChainHead) {
                AssetOffer offer = new AssetOffer(this, call);

                offer.setChainOwner(this);
                offer.setChainId(OrderSide.toInt(OrderSide.SELL));
            } else {
                this.sellOfferChainHead.incorporateOrderCall(call);
            }
        }
        // </editor-fold>
    }

    /**
     * Process buy order call.  If there is situation that transaction occure then it is processed. When any quantity
     * left then it is passed to the chain for incorporation.
     * @param call
     * @throws Exception 
     */
    private void putBuyOffer(OrderCallPut call) throws Exception {
        if (null != this.sellOfferChainHead) {
            AssetOffer currentSell = this.sellOfferChainHead;
            // <editor-fold defaultstate="collapsed" desc="Transaction generation process">
            if (call.getDetails().getPrice() >= currentSell.getPrice()) {
                // there is at least one potencial transaction...
                Boolean done = false;
                OrderCallPut curSellReq = currentSell.getOrderCallChainHead();
                while (false == done) {
                    int amount;
                    int price = currentSell.getPrice();

                    if (curSellReq.getDetails().getCurrentAmount() >= call.getDetails().getCurrentAmount()) {
                        amount = call.getDetails().getCurrentAmount();
                    } else {
                        amount = curSellReq.getDetails().getCurrentAmount();
                    }

                    Transaction tr = new Transaction(
                            getTransactionNextId(),
                            amount,
                            price,
                            this.assetName,
                            call.getBroker(),
                            curSellReq.getBroker(),
                            call.getClient(),
                            curSellReq.getClient()
                    );
                    this.transactions.add(tr);

                    curSellReq = curSellReq.consumeAmount(amount);
                    call.consumeAmount(amount);

                    if (null == curSellReq) {
                        currentSell = null;
                    } else {
                        currentSell = curSellReq.getOffer();
                    }

                    // if 0 then whole order was matched
                    if (0 == call.getDetails().getCurrentAmount()) {
                        done = true;
                    }

                    // null means that all offers have been processed
                    if (null == currentSell) {
                        done = true;
                    } else if (call.getDetails().getPrice() < currentSell.getPrice()) {
                        done = true;
                    }
                }
            }
            // </editor-fold>
        }
        // <editor-fold defaultstate="collapsed" desc="Extend ladder with unconsumer order part">
        // if there is an unconsumed amount then we add it to the ladder
        if (0 < call.getDetails().getCurrentAmount()) {
            if (null == this.buyOfferChainHead) {
                AssetOffer offer = new AssetOffer(this, call);
                try {
                    offer.setChainOwner(this);
                    offer.setChainId(OrderSide.toInt(OrderSide.BUY));
                } catch (ChainException e) {
                    throw e;
                }
            } else {
                this.buyOfferChainHead.incorporateOrderCall(call);
            }
        }
        // </editor-fold>
    }

    /**
     * Provide collection of transactions that took place in asset ladder.
     * @return the mTransactions
     */
    public List<Transaction> getTransactions() {
        return this.transactions;
    }

    /**
     * Provide order book result.
     * @return 
     */
    public OrderBook buildOrderBook() {
        List<OrderEntry> buy = new ArrayList<>();
        List<OrderEntry> sell = new ArrayList<>();

        if (this.buyOfferChainHead == null && this.sellOfferChainHead == null) {
            return null;
        }

        AssetOffer offer = this.buyOfferChainHead;
        int i = 0;
        while (null != offer) {
            OrderCallPut call = offer.getOrderCallChainHead();
            while (null != call) {
                buy.add(new OrderEntry(
                        ++i,
                        call.getBroker(),
                        call.getDetails().getCurrentAmount(),
                        call.getDetails().getPrice(),
                        call.getClient())
                );
                call = (OrderCallPut) call.getNextItem();
            }
            offer = (AssetOffer) offer.getNextItem();
        }

        offer = this.sellOfferChainHead;
        i = 0;
        while (null != offer) {
            OrderCallPut call = offer.getOrderCallChainHead();
            while (null != call) {
                sell.add(new OrderEntry(
                        ++i,
                        call.getBroker(),
                        call.getDetails().getCurrentAmount(),
                        call.getDetails().getPrice(),
                        call.getClient())
                );
                call = (OrderCallPut) call.getNextItem();
            }
            offer = (AssetOffer) offer.getNextItem();
        }

        return new OrderBook(this.assetName, buy, sell);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ChainOwner interface implementation">
    /**
     * Set chain head element.
     * @param newHead
     * @throws ChainException 
     */
    @Override
    public void setChainHead(ChainItem newHead) throws ChainException {
        if (null != newHead) {
            AssetOffer offer = (AssetOffer) newHead;
            if (OrderSide.BUY == offer.getSide()) {
                this.buyOfferChainHead = offer;
            }
            if (OrderSide.SELL == offer.getSide()) {
                this.sellOfferChainHead = offer;
            }
        } else {
            throw new ChainException("Cannot set null as chain head. Use #clearChain instead.", null, this);
        }
    }

    /**
     * Set chain tail element.
     * @param newTail
     * @throws ChainException 
     */
    @Override
    public void setChainTail(ChainItem newTail) throws ChainException {
        if (null != newTail) {
            AssetOffer offer = (AssetOffer) newTail;
            if (OrderSide.BUY == offer.getSide()) {
                this.buyOfferChainTail = offer;
            }
            if (OrderSide.SELL == offer.getSide()) {
                this.sellOfferChainTail = offer;
            }
        } else {
            throw new ChainException("Cannot set null as chain tail. Use #clearChain instead.", null, this);
        }
    }

    /**
     * Clear chain references.
     * @param chainId
     * @throws ChainException 
     */
    @Override
    public void clearChain(int chainId) throws ChainException {
        if (OrderSide.BUY == OrderSide.fromInt(chainId)) {
            this.buyOfferChainHead.setChainOwner(null);
            this.buyOfferChainHead = null;
            this.buyOfferChainTail = null;
        } else if (OrderSide.SELL == OrderSide.fromInt(chainId)) {
            this.sellOfferChainHead.setChainOwner(null);
            this.sellOfferChainHead = null;
            this.sellOfferChainTail = null;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters\getters for class field">
    /**
     * Getter for asset name.
     * @return the assetName
     */
    public String getAssetName() {
        return assetName;
    }

    /**
     * Simple counter for transaction ids.
     * @return 
     */
    private int getTransactionNextId() {
        return ++transactionCount;
    }
    // </editor-fold>
}
