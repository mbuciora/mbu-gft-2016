package com.gft.digitalbank.exchange.solution.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Mock class for chain owner.
 * @author Marcin Buciora (c) 2016
 */
public class ChainOwnerMock implements ChainOwner{
    static Logger logger = LoggerFactory.getLogger(ChainOwnerMock.class);
    private long firstChainId;
    protected ChainItem firstChainHead;
    protected ChainItem firstChainTail;
    private long secondChainId;
    protected ChainItem secondChainHead;
    protected ChainItem secondChainTail;
    
    @Override
    public void setChainHead(ChainItem newHead) throws ChainException {
        switch(newHead.getChainId()){
            case 1:
                this.firstChainHead = newHead;
                break;
            case 2:
                this.secondChainHead = newHead;
                break;
        }
    }

    @Override
    public void setChainTail(ChainItem newTail) throws ChainException {
        switch(newTail.getChainId()){
            case 1:
                this.firstChainTail = newTail;
                break;
            case 2:
                this.secondChainTail = newTail;
                break;
        }
    }

    @Override
    public void clearChain(int chainId) throws ChainException {
        switch(chainId){
            case 1:
                this.firstChainHead = null;
                this.firstChainTail = null;
                break;
            case 2:
                this.secondChainHead = null;
                this.secondChainTail = null;
                break;
            default:
                throw new ChainException("Cannot clear chain. Do not recognize chain id", null, this);
        }        
    }

    /**
     * @return the firstChainHead
     */
    public ChainItem getFirstChainHead() {
        return firstChainHead;
    }

    /**
     * @return the firstChainTail
     */
    public ChainItem getFirstChainTail() {
        return firstChainTail;
    }

    /**
     * @return the secondChainHead
     */
    public ChainItem getSecondChainHead() {
        return secondChainHead;
    }

    /**
     * @return the secondChainTail
     */
    public ChainItem getSecondChainTail() {
        return secondChainTail;
    }
    
}
