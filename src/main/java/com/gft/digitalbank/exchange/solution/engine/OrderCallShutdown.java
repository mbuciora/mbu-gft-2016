package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;

/**
 * Special case of order call used to notify processor that it should end its job.
 * It is a subclass of OrderCall.
 * @see OrderCall
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallShutdown extends OrderCall {
    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Initialize specific properties.
     */
    @Override
    protected void initialize(){}
    
    /**
     * Setup references.
     * @param processor 
     */
    @Override
    public void SetupReferences(BrokerCallProcessor processor) {
    }

    /**
     * Get related asset name.
     * @return 
     */
    @Override
    public String getAssetName() {
        return null;
    }

    /**
     * Process flow.
     * @param ladder
     * @throws Exception 
     */
    @Override
    public void Process(AssetLadder ladder) throws Exception {
    }
    // </editor-fold>
}
