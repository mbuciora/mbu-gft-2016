package com.gft.digitalbank.exchange.solution;

import com.gft.digitalbank.exchange.listener.ProcessingListener;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class StockExchangeTest {

    static Logger logger = LoggerFactory.getLogger(StockExchangeTest.class);

    public StockExchangeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of start method, of class StockExchange.
     */
    @Test
    public void testStartC001() {
        logger.info("start C001: no session active");
        StockExchange se = new StockExchange();
        try {
            se.start();
        } catch (Exception ex) {
            fail("Invalid behaviour.");
        }
    }

    /**
     * Test of start method, of class StockExchange.
     */
    @Test
    public void testStartC002() {
        logger.info("start C002: doubled session start");
        StockExchange se = new StockExchange();
        try {
            se.register(Mockito.mock(ProcessingListener.class));
            se.setDestinations(Arrays.asList("ABC", "DEF", "XYZ"));
            se.start();
            se.start();
        } catch (Exception ex) {
            fail("Invalid behaviour.");
        }
    }

}
