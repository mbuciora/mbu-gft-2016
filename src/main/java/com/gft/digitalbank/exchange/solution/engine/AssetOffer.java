package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;
import com.gft.digitalbank.exchange.solution.tools.ChainException;
import com.gft.digitalbank.exchange.solution.tools.ChainItem;
import com.gft.digitalbank.exchange.solution.tools.ChainOwner;

/**
 * Class represents active offer for specific asset at specific price. It is a part of asset ladder chains.
 * It keeps a chain of orders with specific price any new incoming order call is matched with existing offers.
 * @author Marcin Buciora (c) 2016
 */
public class AssetOffer extends ChainItem implements ChainOwner {
    // <editor-fold defaultstate="collapsed" desc="Class fields">
    /**
     * Order chain head element.
     */
    private OrderCallPut orderCallChainHead;
    /**
     * Order chain tail element.
     */
    private OrderCallPut orderCallChainTail;
    /**
     * Offer side - buy or sell
     */
    private final OrderSide side;
    /**
     * Offer price.
     */
    private final int price;
    /**
     * Offer total amount - sum of current amount of connected orders.
     */
    private int amount;
    /**
     * Reference to parent asset ladder.
     */
    private final AssetLadder asset;
    // </editor-fold>

    public AssetOffer(AssetLadder asset, OrderCallPut call) throws ChainException {
        call.setChainOwner(this);
        this.asset = asset;
        this.side = call.getSide();
        this.price = call.getDetails().getPrice();
        this.amount = call.getDetails().getCurrentAmount();
        call.setStatus(OrderCallStatus.BOOKED);
        call.getContext().setOffer(this);
    }

    // <editor-fold defaultstate="collapsed" desc="Business logic">
    /**
     * Make a cancel one of its orders.
     * @param call
     * @param sourceCall
     * @throws java.lang.Exception
     */
    public void cancelOrderCall(OrderCallPut call, OrderCall sourceCall) throws Exception {
        this.amount -= call.getDetails().getCurrentAmount();
        if (this.amount == 0) {
            excludeFromChain();
        }
        call.setStatus(OrderCallStatus.CANCELL);
        call.excludeFromChain();
        sourceCall.getContext().getProcessor().unregisterOrderCall(call.getId());
    }

    /**
     * Make a modification of connected order.
     * @param call
     * @param sourceCall
     * @throws java.lang.Exception
     */
    public void modifyOrderCall(OrderCallPut call, OrderCallModify sourceCall) throws Exception {
        cancelOrderCall(call, sourceCall);

        OrderCallPut nreq = new OrderCallPut();
        nreq.setBroker(sourceCall.getBroker());
        nreq.setClient(sourceCall.getReferenceOrder().getClient());
        nreq.setMessageType(sourceCall.getReferenceOrder().getMessageType());
        nreq.setId(call.getId());
        nreq.setTimestamp(sourceCall.getTimestamp());
  
        BrokerCallProcessor processor = sourceCall.getContext().getProcessor();
        nreq.getContext().setProcessor(processor);

        nreq.setSide(sourceCall.getReferenceOrder().getSide());

        nreq.setProduct(sourceCall.getReferenceOrder().getProduct());
        nreq.setSourceRequest(sourceCall);
        OrderDetail detail = new OrderDetail();
        detail.setAmount(sourceCall.getDetails().getAmount());
        detail.setPrice(sourceCall.getDetails().getPrice());
        nreq.setDetails(detail);
        processor.registerSubsequentOrderCall(nreq);
        this.asset.processCall(nreq);
    }

    /**
     * Consume amount in case of transaction occurred.
     * @param amount
     * @return
     * @throws IllegalArgumentException
     * @throws ChainException 
     */
    public AssetOffer consumeAmount(int amount) throws IllegalArgumentException, ChainException {
        AssetOffer next = this;
        this.amount -= amount;
        if (0 == this.amount) {
            next = (AssetOffer) getNextItem();
            excludeFromChain();
        }
        return next;
    }

    /**
     * Attach a new order call to offer's order hierarchy. Recurrency methods to built a offer chain.
     * @param call
     * @throws IllegalArgumentException
     * @throws ChainException 
     */
    public void incorporateOrderCall(OrderCallPut call) throws IllegalArgumentException, ChainException {
        AssetOffer offer = this;
        int callPrice = call.getDetails().getPrice();
        if (this.price == callPrice) {
            // add to current item
            this.amount += call.getDetails().getCurrentAmount();
            this.orderCallChainTail.setNextItem(call);
            call.setStatus(OrderCallStatus.BOOKED);
            call.getContext().setOffer(this);
        } else if (this.price > callPrice) {
            if (OrderSide.SELL == call.getSide()) {
                if (null == this.prevItem) {
                    offer = new AssetOffer(this.asset, call);
                    setPrevItem(offer);
                } else {
                    offer = (AssetOffer) this.prevItem;
                    if (offer.getPrice() >= callPrice) {
                        offer.incorporateOrderCall(call);
                    } else {
                        offer = new AssetOffer(this.asset, call);
                        setPrevItem(offer);
                    }
                }
            } else if (OrderSide.BUY == call.getSide()) {
                if (this.nextItem == null) {
                    offer = new AssetOffer(this.asset, call);
                    setNextItem(offer);
                } else {
                    offer = (AssetOffer) getNextItem();
                    if (offer.getPrice() >= callPrice) {
                        offer.incorporateOrderCall(call);
                    } else {
                        offer = new AssetOffer(this.asset, call);
                        setNextItem(offer);
                    }
                }
            }
        } else if (this.price < callPrice) {
            if (OrderSide.SELL == call.getSide()) {
                if (null == this.nextItem) {
                    offer = new AssetOffer(this.asset, call);
                    setNextItem(offer);
                } else {
                    offer = (AssetOffer) getNextItem();
                    if (offer.getPrice() <= callPrice) {
                        offer.incorporateOrderCall(call);
                    } else {
                        offer = new AssetOffer(this.asset, call);
                        setNextItem(offer);
                    }
                }
            } else if (OrderSide.BUY == call.getSide()) {
                if (null == this.prevItem) {
                    offer = new AssetOffer(this.asset, call);
                    setPrevItem(offer);
                } else {
                    offer = (AssetOffer) getPrevItem();
                    if (offer.getPrice() <= callPrice) {
                        offer.incorporateOrderCall(call);
                    } else {
                        offer = new AssetOffer(this.asset, call);
                        setPrevItem(offer);
                    }
                }
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="ChainOwner interface implementation">
    @Override
    /**
     * Set order chain head element.
     */
    public void setChainHead(ChainItem newHead) throws ChainException {
        if (null != newHead) {
            this.orderCallChainHead = (OrderCallPut) newHead;
        } else {
            throw new ChainException("Cannot set null as chain head. Use #clearChain instead.", null, this.chainOwner);
        }
    }

    /**
     * Set order chain tail element.
     * @param newTail
     * @throws ChainException 
     */
    @Override
    public void setChainTail(ChainItem newTail) throws ChainException {
        if (null != newTail) {
            this.orderCallChainTail = (OrderCallPut) newTail;
        } else {
            throw new ChainException("Cannot set null as chain tail. Use #clearChain instead.", null, this.chainOwner);
        }
    }

    /**
     * Clear chain references.
     * @param chainId
     * @throws ChainException 
     */
    @Override
    public void clearChain(int chainId) throws ChainException {
        this.orderCallChainHead.setChainOwner(null);
        this.orderCallChainHead = null;
        this.orderCallChainHead = null;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Setters/getters for class fields">
    /**
     * Getter for side attribute.
     * @return the side
     */
    public OrderSide getSide() {
        return side;
    }

    /**
     * Getter for price.
     * @return the callPrice
     */
    public int getPrice() {
        return price;
    }

    /**
     * Getter for quantity available.
     * @return the quantity
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Returns order chain head element.
     * @return the orderCallChainHead
     */
    public OrderCallPut getOrderCallChainHead() {
        return orderCallChainHead;
    }

    /**
     * Returns order chain tail element.
     * @return the orderCallChainTail
     */
    public OrderCallPut getOrderCallChainTail() {
        return orderCallChainTail;
    }
    // </editor-fold>

}
