package com.gft.digitalbank.exchange.solution.engine;

import com.gft.digitalbank.exchange.model.OrderDetails;
import com.gft.digitalbank.exchange.solution.BrokerCallProcessor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marcin Buciora (c) 2016
 */
public class OrderCallModifyTest {
    static Logger logger = LoggerFactory.getLogger(OrderCallModifyTest.class);
      private OrderCallProvider provider;
      
    public OrderCallModifyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        provider = new OrderCallProvider();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initialize method, of class OrderCallModify.
     */
    @Test
    public void testInitialize() {
        logger.info("initialize");
        
        OrderCallModify order = this.provider.GenerateOrderCallModify(101, 100, 10, "B1");
        order.getDetails().setAmount(100);
        order.initialize();
        assertEquals(100, order.getDetails().getCurrentAmount());
    }

    /**
     * Test of getModifiedOrderId method, of class OrderCallModify.
     */
    @Test
    public void testGetModifiedOrderId() {
        logger.info("getModifiedOrderId");
        OrderCallModify order = new OrderCallModify();
        order.setModifiedOrderId(123);
        assertEquals(123, order.getModifiedOrderId());
    }

    /**
     * Test of setModifiedOrderId method, of class OrderCallModify.
     */
    @Test
    public void testSetModifiedOrderId() {
        logger.info("setModifiedOrderId");
        OrderCallModify order = new OrderCallModify();
        order.setModifiedOrderId(123);
        assertEquals(123, order.getModifiedOrderId());
    }

    /**
     * Test of setDetails method, of class OrderCallModify.
     */
    @Test
    public void testSetDetails() {
        logger.info("setDetails");
        OrderCallModify order = this.provider.GenerateOrderCallModify(101, 100, 10, "B1");
        order.getDetails().setAmount(100);
        order.initialize();
        assertEquals(100, order.getDetails().getCurrentAmount());
    }
    
}
